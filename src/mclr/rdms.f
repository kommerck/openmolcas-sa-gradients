!***********************************************************************
! This file is part of OpenMolcas.                                     *
!                                                                      *
! OpenMolcas is free software; you can redistribute it and/or modify   *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! OpenMolcas is distributed in the hope that it will be useful, but it *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!***********************************************************************
! Copyright (C) 2018 Leon Freitag                                      *
!                                                                      *
! Module that reads, writes and contains state-specific 1-RDMs and     *
! 2-RDMs, as well as 1-TDMs and 2-TDMs                                 *
!***********************************************************************

      module rdms
      Implicit None
      ! One- and two-electron density matrices
      ! Dimensions:
      ! D1(nroot*(nroot+1)/2, nD1) with nD1 = nact*(nact+1)/2
      ! D2(nroot*(nroot+1)/2, nD2) with nD2 = nD1*(nD1+1)/2
      ! D1(itri(I,J), itri(t,u)) I=J: state-specific 1-RDM element D1_tu
      !                          I/=J:transition 1-RDM element D1_tu
      ! i.e. 2nd dimension of D1 and D2 is accessed just as other
      ! 1-RDM and 2-RDM in MOLCAS:
      ! i.e. D1(I,x_ij) = Work(ipG1t+x_ij-1) for one state
      ! if we have multiple states, ipG1t points to the state-average RDM
      ! but D1(:,:) contains state-specific RDMs
      ! everything is also valid for D2
      ! 1-TDM and 2-TDM are symmetrised
      real*8, allocatable :: D1(:,:), D2(:,:)
      ! number of active orbitals (devoid of symmetry),
      ! (second) dimension for D1 and D2
      integer :: nD1, nD2
      ! number of roots
      integer :: nroot
      ! nroot*(nroot+1)/2, ridiculous to declare it explicitly
      integer :: nrootpair
      contains

      ! Functions to access a triangular matrix
      !
      ! MOLCAS itri (used everywhere)
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      integer function itri(i,j)
        integer, intent(in) :: i,j
        itri=Max(i,j)*(Max(i,j)-1)/2+Min(i,j)
      end function itri
!       !
!       ! access a triangular matrix of states
!       !
!       integer function itri_state(i,j)
!         integer, intent(in) :: i,j
!         itri_state=i+nroot*(j-1)
!       end function itri_state
!       !
!       ! access a triangular matrix of orbitals
!       !
!       integer function itri_orb(i,j)
!         integer, intent(in) :: i,j
!         itri_state=i+nact*(j-1)
!       end function itri_orb

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ! initialise the densities: allocate the array and fill the sizes
      ! nroot: number of states
      ! nact: number of active orbitals (cumulative for all symmetries!)
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine init_rdms(nr, nact)
#include "stdalloc.fh"
#include "dmrginfo_mclr.fh"
        integer, intent(in) :: nr, nact
        nroot = nr
        nrootpair = nroot*(nroot+1)/2
        nD1 = nact*(nact+1)/2
        nD2 = nD1*(nD1+1)/2

        ! For now, allocate the state-specific DMs only if we use DMRG
        ! otherwise we don't use it (yet), so we'd waste memory
        ! Deactivated
!         if (doRGLR_MPS) then
!           call mma_allocate(D1, nrootpair, nD1)
!           D1=0.
!           call mma_allocate(D2, nrootpair, nD2)
!           D2=0.
!         end if
      end subroutine init_rdms

      subroutine dealloc_rdms
#include "stdalloc.fh"
        if (allocated(D1)) call mma_deallocate(D1)
        if (allocated(D2)) call mma_deallocate(D2)
      end subroutine dealloc_rdms


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! rddens subroutine from rddens.f, modified to fit in the module.
  ! Reads 1-RDM and 2-RDM (no TDMs!) from JOBIPH and constructs
  ! state-averaged RDMs.
  ! State-average densities are output onto d1_ and d2_, as before
  ! in order not to mess with the program structure.
  ! whereas additionally the diagonal of D1 and D2
  ! i.e. D1(itri(i,i),:) and D2(itri(i,i),:)
  ! is filled with state-specific DMs.
  ! Currently, this is only enabled for DMRG SA gradients.
      Subroutine RdDens(d1_,d2_)
      integer :: i, iB, iDij, iDkl, iijkl,
     &           j, jB, jDisk, kB, lB
      real*8 :: w, fact, rdum
      ! temporary matrices for state-specific 1-RDMs
      ! and the multiplication factor for triangular packing
      real*8, allocatable :: D2t(:), D1t(:), factor(:)
#include "Input.fh"
#include "stdalloc.fh"
#include "SysDef.fh"
#include "Pointers.fh"
#include "WrkSpc.fh"
#include "Files_mclr.fh"
#include "dmrginfo_mclr.fh"
      Real*8 d1_(nd1),d2_(nd2)

      d1_ = 0.0d0
      d2_ = 0.0d0

      call mma_allocate(D2t, nd2)
      call mma_allocate(D1t, nd1)

      ! build up the multiplication factor for triangular packing
      call mma_allocate(factor, nd2)
      factor = 1.0d0
      Do iB=1,ntash
        Do jB=1,iB
          iDij=iTri(ib,jB)
          Do kB=1,ib
          Do lB=1,kB
            iDkl=iTri(kB,lB)
            iijkl=itri(iDij,iDkl)
            if((iDij.ge.iDkl .and. kB.eq.lB).or.
     &       (iDij.lt.iDkl .and. iB.eq.jB)) factor(iijkl)=2.0d00
          End Do
          End Do
        End Do
      End Do

      ! read RDMs
      jDisk = ITOC(3)
      Do i=1,lroots
         W=0.0d0
         Do j=1,nroots
            If (iroot(j).eq.i) W=Weight(j)
         End Do
         Call dDaFile(LUJOB ,2,D1t,nd1,jDisk)
         Call dDaFile(LUJOB ,0,rdum,nd1,jDisk)
         Call dDaFile(LUJOB ,2,D2t,ND2,jDisk)
         Call dDaFile(LUJOB ,0,rdum,ND2,jDisk)

! #ifdef _DMRG_
!          ! for now, only for DMRG
!          if (doRGLR_MPS) then
!             ! fill the diagonal in D1 and D2 RDM arrays
!             D1(itri(i,i),:) = D1t
!             D2(itri(i,i),:) = D2t
!          end if
! #endif
         If (W.ne.0.0d0) Then
            ! form the state-average RDM
            call daxpy_(nd2,w,D2t,1,d2_,1)
            call daxpy_(nd1,w,D1t,1,d1_,1)
         End If
      End Do
      Call Put_D2AV(d2_,nd2)
      Call Put_D1AV(d1_,nd1)

      ! Multiply average 2-RDM with the factor for triangular packing
      d2_ = factor*d2_

#ifdef _DMRG_
      ! For DMRG, read also the transition density matrices
!       call ReadTDMs

      ! Multiply 2-RDMs in D2 with the factor for triangular packing
      ! temporary do it later in FockGen instead
!       if (doRGLR_MPS) then
!         do i=1,lroots
!           do j=1,i
!               D2(itri(i,j),:) = factor(:)*D2(itri(i,j),:)
!           end do
!         end do
!       end if
#endif

      call mma_deallocate(D2t)
      call mma_deallocate(D1t)
      call mma_deallocate(factor)
      Return
      End

      !! Read transition density matrices and save them in the
      !! off-diagonal elements of D1 and D2(:,...)
      !! For now, we only import matrices from QCMaquis DMRG
      !! Uses D1(:,:) and D2(:,:) which are currently deactivated, so beware!

!       Subroutine ReadTDMs
! #ifdef _DMRG_
!         use qcmaquis_interface_wrapper
!         use qcmaquis_info
! #include "Input.fh"
! #include "dmrginfo_mclr.fh"
! #include "stdalloc.fh"
!
!         integer :: i, j, p, q, r, s, pq, rs
!
!         if (doRGLR_MPS) then
!           do i = 1, nroots
!             do j = 1, i-1
!               ! import symmetrised 1-TDM and 2-TDM
!               call dmrg_interface_ctl(
!      &                            task       = 'imp rdmT',
!      &                            x1         = D1(itri(i,j),:),
!      &                            x2         = D2(itri(i,j),:),
!      &                            checkpoint1=
!      &                            qcm_group_names(1)%states(i),
!      &                            checkpoint2=
!      &                            qcm_group_names(1)%states(j),
!      &                            state     = i-1,
!      &                            stateL    = j-1
!      &                     )
!             end do
!           end do
!
!         end if
! #endif
!       end subroutine ReadTDMs

       ! Unfolds 1-RDM and 2-RDM from triangular packing to square packing
       ! as used in fockgen
       ! input: rdm1t, rdm2t: RDMs in triangular packing
       ! mult (optional): if .false., certain elements of 2-RDM will not be
       ! multiplied by 2 when unpacking (default: .true.)

       ! output: rdm1sq, rdm2sq: RDMs in square packing
       ! 1-RDM: full square form, dim = nact*nact
       ! 2-RDM: TDM symmetry form, i.e. with dim = itri(nact**2,nact**2)

       subroutine unfold_rdms(rdm1t,rdm2t,rdm1sq,rdm2sq,mult)

       implicit none

#include "Input.fh"
#include "Pointers.fh"

       real*8 rdm1t(*),rdm2t(*),rdm1sq(*),rdm2sq(*)
       real*8 fact
       integer iB, jB, kB, lB
       integer iDij, iDkl, iRij, iRkl, iijkl, iRijkl

       logical,intent(in), optional :: mult

       logical :: mult_

       mult_ = .true.
       if (present(mult)) then
         if (.not.mult) then
           mult_ = .false.
         end if
       end if

       ! Unfold 1-RDM
       Do iB=1,ntash
       Do jB=1,ntash
         rdm1sq(ib+(jb-1)*ntash)=rdm1t(itri(ib,jb))
       End Do
       End Do

       ! Unfold 2-RDM
       Do iB=1,ntash
       Do jB=1,ntash
         iDij=iTri(ib,jB)
         iRij=jb+(ib-1)*ntash
         Do kB=1,ntash
         Do lB=1,ntash
           iDkl=iTri(kB,lB)
           iRkl=lb+(kb-1)*ntash
           fact=1.0d0
           if (mult_) then
            if(iDij.ge.iDkl .and. kB.eq.lB) fact=2.0d0
            if(iDij.lt.iDkl .and. iB.eq.jB) fact=2.0d0
           end if
           iijkl=itri(iDij,iDkl)
           iRijkl=itri(iRij,iRkl)
           rdm2sq(iRijkl)=fact*rdm2t(iijkl)
         End Do
         End Do
       End Do
       End Do

       end subroutine unfold_rdms


      end module rdms
