************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
************************************************************************
      Subroutine InpCtl_MCLR(iPL)
************************************************************************
*                                                                      *
*     Read all relevant input data and display them                    *
*                                                                      *
*----------------------------------------------------------------------*
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
*                                                                      *
*     history: none                                                    *
*                                                                      *
************************************************************************

      Implicit Real*8 (a-h,o-z)

#include "Input.fh"
#include "Files_mclr.fh"
#include "WrkSpc.fh"
#include "Pointers.fh"
#include "sa.fh"
#include "negpre.fh"

#include "detdim.fh"
#include "csfbas_mclr.fh"
#include "spinfo_mclr.fh"
#include "dmrginfo_mclr.fh"
#include "stdalloc.fh"

      logical ldisk,ipopen

! ==========================================================
!             Used in the DMRGSCF with CI reconstruction
! ==========================================================
      integer,allocatable::index_SD(:) ! not final version
      real*8,allocatable::vector_cidmrg(:)
! ==========================================================

*                                                                      *
************************************************************************
*                                                                      *
      Call Rd1Int ! Read in interesting info from RUNFILE and ONEINT
      Call RdAB   ! Read in orbitals, perturbation type, etc.
*                                                                      *
************************************************************************
*                                                                      *
      Call Rd2Int(iPL)   ! Read in 2el header
*                                                                      *
************************************************************************
*                                                                      *
      Call RdInp_MCLR()  ! Read in input
*                                                                      *
************************************************************************
*                                                                      *
*     Default activate ippage utility

*
      ldisk  =ipopen(0,.True.)
*
      If (iMethod.eq.iCASSCF) Then
         If (TimeDep) Then
            Call RdJobIph_td
         Else
            Call RdJobIph
         End If

         if(doRGLR_cireconstruct)then
           open(unit=117,file="mclr_dets.scratch",
     &        form="unformatted",status="replace")
         end if
!        Write(6,*) 'Setup of Determinant tables'
#ifdef _DMRG_
         if (.not.doRGLR_MPS) then
#endif
          Call DetCtl   ! set up determinant tables

*....... Read in tables from disk
          Call InCsfSD(State_sym,State_sym,.true.)
#ifdef _DMRG_
         end if
#endif
            !Call GetMem('CIvec','Allo','Real',ipNEW,NCONF)
            !Call GetMem('OCIvec','Free','Real',ipCI,nConf)
            !ipCI=ipNEW
*                                                                      *
************************************************************************
*                                                                      *
*        Write(6,*) 'Transformation of CI vector to symmetric '
*    &             ,'group from GUGA pepresentation'

         if(doRGLR_cireconstruct)then
          call flush(117)
          close(117)
          nconf = ncsfs_RGLR
          ncsf(1) = ncsfs_RGLR ! TODO: for now, no symmetry support
          ! allocate ipCI here since we had to get nconf from DetCtl
          ! Otherwise it is allocated in rdjobiph
          Call GetMem('OCIvec','Allo','Real',ipCI,nconf*nroots)

          ! Allocate temporary arrays for CI reconstruction
          call mma_allocate(vector_cidmrg,ndets_RGLR)
          call mma_allocate(index_SD,ndets_RGLR)
        end if

!!! Leon: Enabled CI reconstruction for debugging only for now.
!!! It can be safely disabled anytime.

         if (.not.doRGLR_MPS) then
          Do i=1,nroots
            !! CI reconstruction from DMRG
            if(doRGLR_cireconstruct)then ! No need to copy,since there are no CI-vectors
              !! Leon: For CI reconstruction, the array must have the size of a SD array
              !! even though it's supposed to fit CSFs.
              Call Getmem('CIROOT','ALLO','REAL',ipT,ndets_RGLR)
              call dcopy_(ndets_RGLR,0.0D0,0,WORK(ipT),1)
              index_SD=0
              vector_cidmrg=0.0d0
              write(6,*) "CI coefficient reconstruction: (debug only)"
              call ci_reconstruct(i,ndets_RGLR,vector_cidmrg,
     &                           index_SD)
              iSSM=1     ! yma : side-effect unknown
              call CSDTVC_dmrg(work(ipT),vector_cidmrg,2,WORK(KDTOC),
     &                     index_SD,ISSM,0,IPRDIA)

              call dcopy_(nconf,Work(ipT),1,Work(ipCI+(i-1)*nconf),1)
              Call Getmem('CIROOT','FREE','REAL',ipT,ndets_RGLR)
            else
              Call Getmem('CIROOT','ALLO','REAL',ipT,nconf)
              call dcopy_(nconf,Work(ipCI+(i-1)*nconf),1,Work(ipT),1)
              Call GugaCtl(ipT,1)   ! transform to sym. group
              call dcopy_(nconf,Work(ipT),1,Work(ipCI+(i-1)*nconf),1)
              Call Getmem('CIROOT','FREE','REAL',ipT,nconf)
            end if
          End Do
          if(doRGLR_cireconstruct)then
            call mma_deallocate(vector_cidmrg)
            call mma_deallocate(index_SD)
          End if
         End If
*                                                                      *
************************************************************************
*                                                                      *
        ldisk  =ipopen(nconf,page)
*
*        If we are computing Lagrangian multipliers we pick up all CI
*        vectors. For Hessian calculations we pick up just one vector.
*
         If (SA) Then
            ipcii=ipget(nconf*nroots)
            call dcopy_(nconf*nroots,Work(ipCI),1,Work(ipin(ipcii)),1)
            nDisp=1
         Else
            ipcii=ipget(nconf)
            ipCI_ = ipCI + (iState-1)*nConf
            call dcopy_(nConf,Work(ipCI_),1,Work(ipin(ipcii)),1)
            If (iRoot(iState).ne.1) Then
               Write (6,*) 'McKinley does not support computation of'
     &                   //' harmonic frequencies of excited states'
               Call Abend()
            End If
         End If
C        Call RecPrt('CI vector',' ',Work(ipin(ipcii)),1,nConf)
         Call Getmem('CIVEC','FREE','REAL',ipci,idum)
         ipci=ipcii
         irc=ipout(ipci)
*                                                                      *
************************************************************************
*                                                                      *
         If (ngp) Call rdciv
      End If
*                                                                      *
************************************************************************
*                                                                      *
      Call InpOne           ! read in oneham
      Call PrInp_MCLR(iPL)  ! Print all info
*                                                                      *
************************************************************************
*                                                                      *
      Return
      End
