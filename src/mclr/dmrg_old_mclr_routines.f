************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
************************************************************************

! *********************************************************************
      Subroutine external_dmrg_mclr()

#include "dmrginfo_mclr.fh"

        character(len=255) :: runprefix

!         phaseRHS=.true.  ! Phase check for RHS
!         phaseLAG=.true.  ! Phase check for orbital-only LAG

        call getenv('ORBOPT_PATH',runprefix)
        if (trim(runprefix).eq.'') then
          write (6,*) "WARNING: ORBOPT_PATH is not set."
        end if

        call system(trim(runprefix)//'/orb_opt')
        ! clean up
!         call
      end Subroutine external_dmrg_mclr

! *********************************************************************
      Subroutine MOint_preparation(norbt)

        use hdf5_utils

        integer::  norbt(8)
        integer::  iosh(8)
        integer,allocatable::ireost(:)

        integer nsp,nsq,nsr,nss
        integer lbatch
        integer mbatch
        Real*8, allocatable :: batch(:),h1_in(:)
        real*8 ecore

        integer nsym

        nsym=1  ! Add the symmetry restriction later
        write(6,*)"Attention, currently only for c1 symmetry"
        call flush(6)

      !> Orbital information
        ntot=0
        iosh=0
        do i=1,nsym
          call flush(6)
          iosh(i)=iosh(i)+ntot
          ntot=ntot+norbt(i)
        end do

        write(6,*)"ntot",ntot
        write(6,*)"iosh",iosh
        call flush(6)

        allocate(ireost(ntot))
        do i=1,ntot
          ireost(i)=i
        end do

        write(6,*)"ireost",ireost
        call flush(6)



      !> Assuming there is no symmetry
        lupri=6
        nnashx=norbt(1)*(norbt(1)+1)/2

      !> initialize HDF5 support and open the files nevpt.h5 and ijkl.h5
        call hdf5_init()
        call hdf5_open(ijklname, file_id(2))

        ! Core energy
        datadim(1)   = 1; datadim_bound = 1
        call hdf5_get_data(file_id(2),"ecore ",datadim,ecore)
        write(6,*)"The core energy",ecore
        call flush(6)

        ! 1-e integrals
        allocate(h1_in(nnashx)); h1_in = 0
        datadim(1)   = nnashx; datadim_bound = 1
        call hdf5_get_data(file_id(2),"FockMO",datadim,h1_in)

        ! 2-e integrals
        nsp=1; nsq=1; nsr=1; nss=1
        write(lupri,'(a,4i1,a/)')
     &  ' sorting ints for integral class: (',nsp,nsq,nsr,nss,')'
        call flush(lupri)

        !> non-zero (by symmetry) batch of integrals
        write(datatag,'(a1,i3,a1,i3,a1,i3,a1,i3)')
     &  'p',nsp,'q',nsq,'r',nsr,'s',nss
        tagx = 16
        datadim(1)        = 1; datadim_bound = 1
        call hdf5_get_data(file_id(2),"XXXXXX",datadim,mbatch)

!        write(lupri,*)"mbatch",mbatch
        lbatch            = mbatch

!        write(lupri,*) ' # integrals ==> ',lbatch
!        call flush(lupri)

        allocate(batch(lbatch)); batch = 0

        datadim(1)        = lbatch
        call hdf5_get_data(file_id(2),"XXXXXX",datadim,batch)

!        write(lupri,*) ' batch is ... ',batch
!1        call flush(lupri)


        open(unit=117,file="FCIDUMP") ! temporary still use the ASCII file
          write(117,"(A12,I3,A11)")" &FCI norbt= ",ntot,",NELEC=  2,"
          write(117,"(A9)",advance='no')"  ORBSYM="
          do i=1,nsym
            do j=1,norbt(i)
              write(117,"(I2,A1)",advance='no')i,","
            end do
          end do
          write(117,*)
          write(117,"(A8)")"  ISYM=1"
          write(117,"(A5)")" &END"



          indtot = 0  ! for all index
              indn = 0  ! for index in a symmetry irreps

              do
!#ifdef IJKL_DEBUG
!                write(lupri,*) 'indn is ... ',indn
!                call flush(lupri)
!#endif
                if(indn >= lbatch) exit

                !> determine external indices i,j,k,l
                do k  = 1, norbt(nsr)
                  ktmp = norbt(nss)
                  if(nss == nsr) ktmp = k

                  do l = 1, ktmp
                    istart = 1
                    if(nsr == nsp) istart = k
                    if(nsp == nsr)then
                      noccendi = k
                    else
                      noccendi = norbt(nsp)
                    end if

                    do i = istart, norbt(nsp)
                      jstart = 1
!                     if IJKL in same irrep, restrict j to at most i
                      if(nsq==nsp.and.nsr==nss)then !.and.ISYM == KSYM
                        noccendj = i
!                       if LJ in irrep1 and IK in irrep2
                      else if(nss==nsq.and.nsr==nsp.and.nss/=nsp)then
!                       second restriction to prevent double counting, J<=L in KL IJ
                        if(k==i)then
                          jstart   = l
                          noccendj = norbt(nsq)
                        else ! otherwise all J are needed
                          noccendj = norbt(nsq)
                        end if
                      else
                        noccendj = norbt(nsq)
                      end if

                      do j = jstart, noccendj
!                       write (lupri,*) 'i,j,k,l            ',i,j,k,l
                        if(nsp==nsq.and.nsr==nss.and.nsp==nsr)then
                          if(k==i.and.l==i.and.j<i) cycle
                          if(k==i.and.j<l) cycle
                        end if
                        indn   = indn   + 1
                        indtot = indtot + 1

!                        write(6,*)"i,j,k,l",i,j,k,l
!                        call flush(6)

                        ii = ireost(i+iosh(nsp))
                        jj = ireost(j+iosh(nsq))
                        kk = ireost(k+iosh(nsr))
                        ll = ireost(l+iosh(nss))

!                        write(6,*)"ii,jj,kk,ll",ii,jj,kk,ll
!                        call flush(6)

                        if(jj > ii)then
                          itamp = ii
                          ii    = jj
                          jj    = itamp
                        end if
                        if(ll > kk)then
                          itamp = kk
                          kk    = ll
                          ll    = itamp
                        end if
                        !> swap indices if k,l > i,j
                        if(kk > ii)then
                          itamp  = kk
                          itamp2 = ll
                          kk     = ii
                          ll     = jj
                          ii     = itamp
                          jj     = itamp2
                        end if
                        !> determine internal ij,kl
!                        IJ = nevpt_ijkl%num(ii)+jj
!                        KL = nevpt_ijkl%num(kk)+ll
!#ifdef IJKL_DEBUG
!                  write (lupri,*) 'indtot, indn', indtot, indn
!                  write (lupri,*) 'ii,jj,kk,ll',ii,jj,kk,ll,batch(indn)
!                      call flush(lupri)
                  write (117,112)batch(indn),kk,ll,ii,jj
!#endif
!                        if (ij < kl)then ! swap indices
!                          itamp = ij
!                          ij    = kl
!                          kl    = itamp
!                        endif

!                      if(j.eq.noccendj)then
!                        write(lupri,*)
!                        stop
!                      end if

                      end do
                    end do
                  end do
                end do
              end do
112     format(G21.12,I4,I4,I4,I4)

              deallocate(batch)
              deallocate(ireost)


              write(lupri,*) ' The T(j,i) with j>i '
              ij=0
              do i=1,norbt(1)
                do j=1,i
                  ij=ij+1
                  write(lupri,*)h1_in(ij),i,j
                  write(117,112)h1_in(ij),i,j,0,0
                end do
              end do

              write(117,112)ECORE,0,0,0,0

            close(117)

        write(6,*)"norbt in MOint_preparation",norbt
        call flush(6)
!        stop
        !> close the files nevpt.h5+ijkl.h5 and turn-off HDF5 support
        call hdf5_close(file_id(2))
        call hdf5_exit()

        write(6,*)"finished the hdf5 file"
        call flush(6)

      end Subroutine MOint_preparation

! *********************************************************************
      subroutine read_dmrg_parameter_for_mclr()

#include "Input.fh"
#include "dmrginfo_mclr.fh"
          integer ierr,i
        character cI,cII
        Character*8 Method

        doRGLR_MPS = .false.
        Call Get_cArray('Relax Method',Method,8)
        if (Method.eq.'DMRGSCFS') then
          call get_lScalar('CIRS', doRGLR_cireconstruct)
          if (.not. doRGLR_cireconstruct) doRGLR_MPS = .true.
        end if

        ! Set the global DMRG gradient flag
        doRGLR = doRGLR_MPS.or.doRGLR_cireconstruct
     !! TODO: Remove this file and read all info from qcmaquis_info module
!         open(unit=100,file="dmrg_for_mclr.parameters",
!      &       status='OLD',action='READ',iostat=ierr)
!      !! WARNING:
!      !! this fails when a CASSCF has been performed in scratch where the dmrg_for_mclr.parameters
!      !! file exists from a previous calculation !!
!         close(100)

        ! Set the global DMRG gradient flag
!         doRGLR = doRGLR_hybrid.or.doRGLR_MPS
      end subroutine read_dmrg_parameter_for_mclr

! *********************************************************************
      Subroutine import_orb_LAG(lag_orb,lorb)

#include "dmrginfo_mclr.fh"

        integer :: lorb
        real*8 :: lag_orb(*)
        real*8 dv

        dv=1.0d0
!         if(.not.phaseRHS)then
!           if(.not.phaseLAG)then
!             dv=-1.0d0
!             write(6,*)"phaseRHS, phaseLAG", phaseRHS, phaseLAG
!             write(6,*)"Sign changed for orb_LAG"
!           end if
!         end if

        open(unit=110,
     &       file="orb_lag.txt")
          do i=1,lorb
            read(110,*)ij,lag_orb(i)
            lag_orb(i)=lag_orb(i)*dv
          end do
        close(110)


      End Subroutine import_orb_LAG

! ---------------------------------------------------------------------
      Subroutine import_MPS_LAG(lag_mps1,lag_mps2,lmps1,lmps2)

#include "dmrginfo_mclr.fh"

        integer:: lmps1,lmps2
        real*8 :: lag_mps1(*),lag_mps2(*)
        real*8 dv

! Leon: temporarily set the sign to -1 -- until we find an example where it doesn't work
        dv=-1.0d0
!         if(phaseRHS.and..not.phaseLAG)then
!           dv=-1.0d0
!           write(6,*)"phaseRHS, phaseLAG", phaseRHS, phaseLAG
          write(6,*)"Sign changed for MPS_LAG"
!         end if

        open(unit=110,
     &       file="1RDM_lag.txt")
          do i=1,lmps1
            read(110,*)ij,lag_mps1(i)
            lag_mps1(i)=lag_mps1(i)*dv
          end do
        close(110)

        open(unit=110,
     &       file="2RDM_lag.txt")
          do i=1,lmps2
            read(110,*)ij,lag_mps2(i)
            lag_mps2(i)=lag_mps2(i)*dv
          end do
        close(110)


      End Subroutine import_MPS_LAG

! *********************************************************************

      ! Recover the 2'-RDM-LAG (MPS), full matrix form
      Subroutine import_MPS_LAG2(lag_mps2m,lmps2m)

        integer::lmps2m
        real*8 :: lag_mps2m(*)
        real*8 dv
! Leon: temporarily set the sign to -1 -- until we find an example where it doesn't work
        dv=-1.0d0
        open(unit=110,file="2RDM_lag_full.txt")
          do i=1,lmps2m
            read(110,*)ij,lag_mps2m(i)
            lag_mps2m(i)=lag_mps2m(i)*dv
          end do
        close(110)

      end Subroutine import_MPS_LAG2


! *********************************************************************

      Subroutine ci_reconstruct(istate,nSDET,vector,indexSD)
      use qcmaquis_info
#include "dmrginfo_mclr.fh"
#include "stdalloc.fh"
#include "Input.fh"
!        character*100,allocatable :: qcm_group_names(1)%states(:)  ! for many states
        character*255 :: qcmaquis_lr_path
        character*255 :: runstring
        character*255 :: fmt_el_conf ! electronic configuration as string
        integer :: istate,nsdet
        integer, parameter :: tmpfile = 1737

        integer i,j,idet,idx_det
        integer norbt,norbtLR

!        integer neletol
!        integer nele_mod
!        integer nele_alpha,nele_beta

        integer irrep_pre,iorbLR0,iorbLR
        integer irrep_diff(8)

! need to be rewrittren using the Molcas getmem,
!                                    or mma_allocate and mma_deallocate
! soon ..
        integer,allocatable::ele_orb_alpha(:)
        integer,allocatable::ele_orb_beta(:)

        integer,allocatable::pre_ele(:)

        integer ndets_mclr

        character*200 tmp_run

        real*8 dtmp

        logical IFFILE
        integer rc

        integer :: indexSD(nsdet)            ! index
        real*8 :: vector(nsdet)    ! determinants
        type Slater_determinant
          integer :: itype                        = 1 ! excitation type
          integer :: inum                         = 1 ! determinant number
          integer :: isign                        = 1 ! determinant phase
          integer,allocatable :: electron(:)          ! determinant
          integer,allocatable :: ele_conf(:)          ! electron configuration
          real*8 ,allocatable :: dV(:)                ! for many states
        end type Slater_determinant

        type (Slater_determinant), allocatable::SD_DMRG(:)

        character(len=30) :: str
        character(len=300), allocatable :: chk_tmp(:)
        external str

          rc = 0

          call mma_allocate(chk_tmp, nstates_RGLR)
! Some debugging information
!#ifdef _DMRG_DEBUG_
          write(6,*)"Enter the ci_reconstruct"
          write(6,*)"istate",istate,"nSDET",nSDET
          call flush(6)
!#endif

          !> The total electrons
          neletol=0
          neletol= nele_RGLR
!
!          !> Check if there is single electron
!          nele_mod=mod(neletol,2)

!          !> electrons in alpha or beta orbitals
!          if(nele_mod.eq.0)then
!            !> If no single electron
!            nele_alpha= neletol/2 + ms2_RGLR/2
!            nele_beta = neletol/2 - ms2_RGLR/2
!          else
!            nele_alpha= neletol/2 + ms2_RGLR/2+1
!            nele_beta = neletol/2 - ms2_RGLR/2
!          end if

! Read in all the name of checkpoint file
!          lcheckpoint=20
!          lcheckpoint=isFreeUnit(lcheckpoint)
!          Call Molcas_Open(lcheckpoint,'dmrg_for_mclr.parameters')
!          allocate(qcm_group_names(1)%states(nstates_RGLR))
!          checkpoint=""
!          do i=1,3
!            read(lcheckpoint,*)
!          end do
!          do i=1,nstates_RGLR
!            read(lcheckpoint,*)qcm_group_names(1)%states(i)
!            read(lcheckpoint,*)
!          end do
!          close(lcheckpoint)

          ! reconstructing dets for current state
!#ifdef _DMRG_DEBUG_
          write(6,*)" Transforming DMRG checkpoints into 2u1 symmetry"
!#endif
          call system("mps_transform_pg "//
     &     trim(qcm_group_names(1)%states(istate)))
          ! Very ugly hack to get the 2u1 checkpoint name
          ! without knowing the spin. Doesn't work for higher spins
          ! but we don't care
          call system("ls -1 -d $(dirname "//
     &              trim(qcm_group_names(1)%states(istate))//
     &              ")/*checkpoint_state."//trim(str(istate-1))//
     &              ".?.?.h5 | head -1> resulttmpfile", status=rc)
          if (rc.ne.0) stop "Failed getting 2u1 checkpoint name file"
          open(tmpfile, file="resulttmpfile", status="old")
          read(tmpfile, '(A)', iostat=rc) chk_tmp(istate)
          if (rc < 0) continue
          close(tmpfile, status="delete")
          write(6,*)trim(chk_tmp(istate))
          call flush(6)

          ! preparing for point group symmetry
          norbt=0
          norbtLR=0
          irrep_diff=0
          do j=1,8
             norbt          =  norbt      +   nash(j)
             norbtLR        =  norbtLR    +   nash(j)
             irrep_diff(j) =  0
          end do

          call mma_allocate(pre_ele,norbtLR); pre_ele=0
          iorbLR0=1
          iorbLR =0
          irrep_pre=0
          do i=1,8
            iorbLR=iorbLR+nash(i)
            if(i.eq.1)then
            else
              irrep_pre=irrep_pre+irrep_diff(i-1)
            end if
            do j=iorbLR0,iorbLR,1
              pre_ele(j)=irrep_pre
              write(6,*)"j,pre_ele(j)",j,pre_ele(j)
            end do
            iorbLR0=iorbLR+1 ! At least work for C1
          end do
!          write(*,*)"the ndets_RGLR in re-construction", ndets_RGLR

          !> DETs read from mclr_dets.scratch
          open(UNIT=117,file="mclr_dets.scratch",form="unformatted",
     &         status="OLD")
! Does the mma_allocate not work for "type/class" ? -- yma
!          call mma_allocate(SD_DMRG,ndets_RGLR)
          allocate(SD_DMRG(ndets_RGLR))
          do idet=1,ndets_RGLR
            call mma_allocate(SD_DMRG(idet)%electron,neletol)
            call mma_allocate(SD_DMRG(idet)%ele_conf,norbt)
            call mma_allocate(SD_DMRG(idet)%dv,nstates_RGLR)
            SD_DMRG(idet)%electron=0
            SD_DMRG(idet)%ele_conf=0
            SD_DMRG(idet)%dv=0.0d0
            !read(117,"(1X,I8,6X)",advance='no')SD_DMRG(idet)%ITYPE
            read(117)SD_DMRG(idet)%ITYPE
!            write(6,*)SD_DMRG(idet)%ITYPE
!            call flush(6)
            do i=1,neletol
              !read(117,"(1X,I5)",advance='no')SD_DMRG(idet)%electron(i)
              read(117)SD_DMRG(idet)%electron(i)
!              write(6,*)SD_DMRG(idet)%electron(i)
!              call flush(6)
            end do
            !read(117,"(5X,I3,11X)",advance='no')SD_DMRG(idet)%isign
            read(117)SD_DMRG(idet)%isign
!            write(6,*)SD_DMRG(idet)%isign
!            call flush(6)
            !read(117,*)SD_DMRG(idet)%inum
            read(117)SD_DMRG(idet)%inum
!            write(6,*)SD_DMRG(idet)%inum
!            call flush(6)
#ifdef _DMRG_DEBUG_
            write(6,*)         "idet",        idet            ! yma
            write(6,*)"SD_DMRG ITYPE",SD_DMRG(idet)%ITYPE
            write(6,*)"SD_DMRG ELECT",SD_DMRG(idet)%electron
            write(6,*)"SD_DMRG ISIGN",SD_DMRG(idet)%isign
            write(6,*)"SD_DMRG INUM ",SD_DMRG(idet)%inum
#endif
          end do
          close(117)

!          write(6,*)"before get the executable file"
!          call flush(6)

          !> get the executable file

          call mma_allocate(ele_orb_alpha,norbt)
          call mma_allocate(ele_orb_beta,norbt)
          !> All of the DETs into Maquis format
          !> ASCII file used, need to be updated - yma
          open(unit=118,file="dets.mclr")
          do idet=1,ndets_RGLR
            ele_orb_alpha=0
            ele_orb_beta=0
            do i=1,neletol
              if(SD_DMRG(idet)%electron(i)>0)then ! With preconditioner
                j=abs(SD_DMRG(idet)%electron(i))
                ele_orb_alpha(j+pre_ele(j))=1
              else
                j=abs(SD_DMRG(idet)%electron(i))
                ele_orb_beta(j+pre_ele(j))=1
              end if
            end do
            !> The same style also used in Maquis input
            do i=1,norbt
              if(ele_orb_alpha(i).eq.1.and.ele_orb_beta(i).eq.1)
     &           SD_DMRG(idet)%ele_conf(i)=4
              if(ele_orb_alpha(i).eq.1.and.ele_orb_beta(i).eq.0)
     &           SD_DMRG(idet)%ele_conf(i)=3
              if(ele_orb_alpha(i).eq.0.and.ele_orb_beta(i).eq.1)
     &           SD_DMRG(idet)%ele_conf(i)=2
              if(ele_orb_alpha(i).eq.0.and.ele_orb_beta(i).eq.0)
     &           SD_DMRG(idet)%ele_conf(i)=1
              !write(6,*)"SD_DMRG(idet)%ele_conf(i)",SD_DMRG(idet)%ele_conf(i)
            end do
            do i=1,norbt
              write(118,"(I1)",advance='no')SD_DMRG(idet)%ele_conf(i)
!              write(6,*)"SD_DMRG(idet)%ele_conf(",i,")",
!     &                   SD_DMRG(idet)%ele_conf(i)
            end do
            write(118,*)
          end do
          close(118)
          call mma_deallocate(ele_orb_alpha)
          call mma_deallocate( ele_orb_beta)

          !> Test for part of DETS
          !> The way of "open file" need to be written
          !                                   Yingjin 2015.8.13
          call system("wc -l dets.mclr > dets.mclr.info")
          open(unit=118,file="dets.mclr.info")
            read(118,*)ndets_total
          close(118,status="delete")

          !> Recover the determinants, off-diagional multiply 2
          !> ========= should be improved by Hash etc. ==========
          do i=istate,istate
             !write(6,*)"SD_DMRG(idet)%dv(i)",i
            write(6,*)"qcm_group_names(1)%states(i)",i,
     &                trim(qcm_group_names(1)%states(i))
            call flush(6)

            call getenv("QCMAQUIS_LR_PATH",qcmaquis_lr_path)
            if (trim(qcmaquis_lr_path)=='') then
              write(6,*) "WARNING: QCMAQUIS_LR_PATH is not set"
            end if
            runstring="srcas_2u1pg "
            open(unit=118,file="GET_COEFF_IN_LIST")
            tmp_run=trim(runstring)//" "//trim(chk_tmp(i))//
     &               " dets.mclr 1.0 1.0 0 dets.mclr > "//"CIRE.scratch"
            write(118,*)trim(tmp_run)
            close(118)
            call systemf("chmod +x GET_COEFF_IN_LIST",rc)
            call systemf("./GET_COEFF_IN_LIST",rc)
            if (rc.ne.0) stop "Running CI reconstruction (srcas) failed"

            !> read in the dets-coefficients
            open(unit=118,file="det_coeff.tmp")
            read(118,*)ndets_mclr
            do idet=1,ndets_mclr
              read(118,*)idx_det,SD_DMRG(idx_det)%dv(i)
!              write(6,*)idx_det,SD_DMRG(idx_det)%dv(i)
            end do
            close(118)
            !>  off-diagional multiply 2
            do idet=1,ndets_RGLR
              if(SD_DMRG(idet)%ITYPE.eq.1)then
                SD_DMRG(idet)%dv(i)=-1.0d0*SD_DMRG(idet)%dv(i)
              else
                dtmp=sqrt((SD_DMRG(idet)%dv(i)**2)*2.0d0)
                SD_DMRG(idet)%dv(i)=
     &         -1.0d0*dsign(dtmp,SD_DMRG(idet)%dv(i))
              end if

              fmt_el_conf = ""
              do iii=1,norbt
              ! Very very ugly conversion of the slater det. to string
                fmt_el_conf=trim(fmt_el_conf)//
     &             char(SD_DMRG(idet)%ele_conf(iii)+48)
              end do
              write(6,'(A,I2,X,I2,X,A,X,F13.10,X,I3)')
     &          "i,idet,conf,dv,ITYPE",i,idet,
     &          trim(fmt_el_conf),SD_DMRG(idet)%dv(i),
     &           SD_DMRG(idet)%ITYPE
            end do
          end do

          dtmp=0.0d0
          indexSD=0
          vector=0.0d0
          do i=1,ndets_RGLR
            indexSD(i)=i
!            indexSD(i)=i !SD_DMRG(i)%inum*SD_DMRG(i)%isign
            vector(i)=SD_DMRG(i)%dv(istate)
            dtmp=dtmp+vector(i)**2
          end do

!          write(*,*)"nele_alpha","nele_beta",nele_alpha,nele_beta
          write(6,*)
          write(6,*)"Total CI weight in re-construction is ", dtmp
          write(6,*)
          write(6,*)" ================================================ "
          call flush(6)

          !> delete the transformed checkpoint file
          do i=istate,istate
            call system("rm -rf "//trim(chk_tmp(i)))
          end do

          call mma_deallocate(pre_ele)
          do idet=1,ndets_RGLR
            call mma_deallocate(SD_DMRG(idet)%electron)
            call mma_deallocate(SD_DMRG(idet)%ele_conf)
            call mma_deallocate(SD_DMRG(idet)%dv)
          end do
          deallocate(SD_DMRG)
          if (allocated(chk_tmp)) call mma_deallocate(chk_tmp)

      end Subroutine ci_reconstruct

      character(len=30) function str(k)
      ! Quick integer-to-string converter, not to mess with the conversion somewhere else.
      ! From https://stackoverflow.com/questions/1262695/convert-integers-to-strings-to-create-output-filenames-at-run-time
          integer, intent(in) :: k
          write (str, *) k
          str = adjustl(str)
      end function str
