************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
************************************************************************
      Subroutine RdJobIph
************************************************************************
*                                                                      *
*     Read the contents of the JOBIPH file.                            *
*                                                                      *
*----------------------------------------------------------------------*
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
*                                                                      *
*     history: none                                                    *
*                                                                      *
************************************************************************
      use rdms
#ifdef _DMRG_
      use qcmaquis_info
      use qcmaquis_interface_environment
      use qcmaquis_interface_measurements
      use qcmaquis_interface_main, only: qcmaquis_mps_multicanonize
      use dmrg_mini_interface
#endif
      Implicit Real*8 (a-h,o-z)

#ifdef _DMRG_
#include "stdalloc.fh"
      ! Temporary output checkpoints for multi_canonize
       character(len=256), allocatable :: out_checkpoints(:)

#endif

#ifdef _HDF5_
#include "mh5.fh"
      integer :: refwfn_id
#endif

#include "Input.fh"
#include "Files_mclr.fh"
#include "glbbas_mclr.fh"
#include "disp_mclr.fh"
#include "Pointers.fh"
#include "WrkSpc.fh"
#include "SysDef.fh"
#include "sa.fh"
#include "dmrginfo_mclr.fh"
#include "warnings.fh"
      Character*72 Line
      Character*8 Method
      real*8 dv_ci2  ! yma added
      Logical Found
      Character(*), parameter :: h5name = 'RASWFN'

*                                                                      *
************************************************************************
*                                                                      *
*     itri(i,j)=Max(i,j)*(Max(i,j)-1)/2+Min(i,j)
*                                                                      *
************************************************************************
*                                                                      *
*define _DEBUG_
      debug=.FALSE.
#ifdef _DEBUG_
      debug=.TRUE.
#endif

*                                                                      *
************************************************************************
*                                                                      *
*     Save the ROOT input parameter                                    *
*----------------------------------------------------------------------*
      kRoots=lRoots
*----------------------------------------------------------------------*
*     Read the table of disk adresses                                  *
*----------------------------------------------------------------------*
      Call DaName(LuJob,FnJob)
      iDisk=0
      Call iDaFile(LuJob,2,iToc,iTOCIPH,iDisk)
*----------------------------------------------------------------------*
*     Read the the system description                                  *
*----------------------------------------------------------------------*
      ndum=lenin8*mxorb/itob
      Call Getmem('TMP','ALLO','INTE',ipdum,ndum)
      iDisk=iToc(1)

!      write(*,*)"if dmrg, it should be something else ",nconf  ! yma
      Call WR_RASSCF_Info(LuJob,2,iDisk,
     &                    nActEl,iSpin,nSym,State_sym,nFro,
     &                    nIsh,nAsh,nDel,
     &                    nBas,MxSym,iwork(ipdum),LENIN8*mxorb,
     &                    nConf,HeaderJP,144,
     &                    TitleJP,4*18*mxTit,PotNuc0,lRoots,
     &                    nRoots,iRoot,mxRoot,
     &                    nRs1,nRs2,nRs3,
     &                    nHole1,nElec3,iPt2,Weight)

      Call Getmem('TMP','FREE','INTE',ipdum,ndum)
*----------------------------------------------------------------------*
*     Overwrite the variable lroots if approriate, i.e if lroot        *
*     was set by input.                                                *
*----------------------------------------------------------------------*
      If ( kRoots.ne.-1 ) then
         If ( iPt2.ne.0 ) then
            Write (6,*) 'RdJobiph: kRoots.ne.-1 .and. iPt2.ne.0'
            Call Abend()
         Else if ( kRoots.gt.lRoots ) then
            Write (6,*) 'RdJobiph: kRoots.ne.-1 .and. kRoots.gt.lRoots'
            Call Abend()
         End If
         lRoots=kRoots
         nRoots=1
      End If
*----------------------------------------------------------------------*
*     Precompute the total sum of variables and size of matrices       *
*----------------------------------------------------------------------*
      ntIsh=0
      ntItri=0
      ntIsqr=0
      ntAsh=0
      ntAtri=0
      ntAsqr=0
      ntBas=0
      ntBtri=0
      ntBsqr=0
      nna=0
      Do 10 iSym=1,nSym
         norb(isym)=nbas(isym)-ndel(isym)
         ntIsh=ntIsh+nIsh(iSym)
         ntItri=ntItri+nIsh(iSym)*(nIsh(iSym)+1)/2
         ntIsqr=ntIsqr+nIsh(iSym)*nIsh(iSym)
         ntAsh=ntAsh+nAsh(iSym)
         ntAtri=ntAtri+nAsh(iSym)*(nAsh(iSym)+1)/2
         ntAsqr=ntAsqr+nAsh(iSym)*nAsh(iSym)
         ntBas=ntBas+nBas(iSym)
         ntBtri=ntBtri+nBas(iSym)*(nBas(iSym)+1)/2
         ntBsqr=ntBsqr+nBas(iSym)*nBas(iSym)
         nA(iSym)=nna
         nnA=nnA+nAsh(isym)
10    Continue

* for DMRG: Read QCMaquis checkpoint names from .rasscf.h5 file
#ifdef _DMRG_
      if (doRGLR_MPS) then
#ifdef _HDF5_
        ! Open the .rasscf.h5 file
        ! For now, we will only read QCMaquis checkpoint file names from
        ! HDF5, the rest is still read from RunFile.
        ! TODO: Implement reading of everything from HDF5!! (as in RASSI)
        If (mh5_is_hdf5(h5name)) Then
          refwfn_id = mh5_open_file_r(h5name)

          write (6,*) " Reading QCMaquis checkpoint file names "//
     &                "from HDF5 file"

          ! initialise array with QCMaquis checkpoint names
          call qcmaquis_info_init(1,nroots,-1)
          do ist = 1, nroots
            if(mh5_exists_dset(refwfn_id, 'QCMAQUIS_CHECKPOINT')) then
              call mh5_fetch_dset_array_str(refwfn_id,
     &                                    'QCMAQUIS_CHECKPOINT',
     &                                     qcm_group_names(1)
     &                                     %states(ist),
     &                                     [1],
     &                                     [ist-1]
     &                                    )
              write (6,'(a,i4,a,a)') " State ",ist,
     &           " ... ",
     &          trim(qcm_group_names(1)%states(ist))
            else
              call WarningMessage(2,
     &         "QCMAQUIS_CHECKPOINT record not found in HDF5 file.")
              call Quit_OnUserError()
            end if
          end do
          call mh5_close_file(refwfn_id)
        else
          call WarningMessage(2, "Cannot open rasscf.h5 file.")
          call Quit_OnUserError()
        endif

        if (dmrg_lrsite.ge.nash(1)) then
          write(6,*) "Site for LR parameters is too large! "//
     &               "You specified "//trim(str(dmrg_lrsite))//
     &       ", maximum allowed is "//trim(str(nash(1)-1))
          write(6,*)"(note that the site numbering starts with 0 here)"
          call Quit_OnUserError
        end if

        ! If lrsite hasn't been set, set it to the middle of the lattice
        if (.not.set_lrsite) then
          dmrg_lrsite = (nash(1)-1)/2
        end if

        call initialize_dmrg_mclr(sum(nash),nactel,state_sym,ispin-1,
     &                            nroots,dmrg_lrsite)


        call mma_allocate(out_checkpoints,nroots)
        if (dmrg_lrsite.gt.0) then
          write(6,*) "Performing MPS multi-canonicalization at site "
     &                //trim(str(dmrg_lrsite))
          call qcmaquis_mps_multicanonize(dmrg_lrsite,
     &                      qcm_group_names(1)%states,
     &                      out_checkpoints)

          write(6,*) "Checkpoint names for multi-canonized MPS: "
          do ist = 1, nroots
            write (6,'(a,i4,a)') " State ",ist,
     &           " ... "//trim(out_checkpoints(ist))
            qcm_group_names(1)%states(ist) = out_checkpoints(ist)
          end do
        end if



        call mma_deallocate(out_checkpoints)
#else
        call WarningMessage(2,
     &     "DMRG SA gradient calculation requires HDF5 support!")
        call Quit_OnUserError()
#endif
      endif
#endif

*
*----------------------------------------------------------------------*
*     Load the orbitals used in the last macro iteration               *
*----------------------------------------------------------------------*
*
      Call Get_CMO(ipCMO,Length)
C
C     Read state for geo opt
C
      Call Get_iScalar('Relax CASSCF root',irlxroot)
      Call Get_cArray('Relax Method',Method,8)
      iMCPD=.False.
      if(Method.eq.'MCPDFT  ')iMCPD=.True.
      If (Method.eq.'CASSCFSA'.or.Method.eq.'DMRGSCFS') Then
         Call Get_iScalar('SA ready',iGo)
         If (iGO.eq.-1) Then
            Write (6,*) 'MCLR not implemented for SA-CASSCF'//
     &                  ' with non-equivalent weights!'
            Call Abend()
         Else
            If (iGo.ne.2) SA=.true.
            Found=.true.
            If (override) Then
               If (isNAC) Then
                  Do j=1,2
                    NSSA(j)=0
                    Do i=1,lroots
                       If (iroot(i).eq.NACStates(j)) NSSA(j)=i
                    End Do
                    If (NSSA(j).eq.0) Found=.false.
                  End Do
               Else
                  irlxroot=iroot(istate)
               End If
            Else
               istate=0
               Do i=1,lroots
                  If (iroot(i).eq.irlxroot) istate=i
               End Do
               If (istate.eq.0) Found=.false.
            End If
            If (.not.Found) Then
               Call WarningMessage(2,
     &              'Cannot relax a root not included in the SA')
            End If
         End If
      Else If (irlxroot.eq.1.and..Not.(McKinley.or.PT2.or.iMCPD)) Then
         Write (6,*)
         Write (6,*) 'W A R N I N G !'
         Write (6,*)
         Write (6,*) 'Redundant rlxroot input in RASSCF!'
         Write (6,*) 'I''ll sign off here without a clean termination!'
         Write (6,*) 'However, I have to fix the epilogue file.'
         Write (6,*)
         irc=-1
         iopt=1
         Call OPNMCK(irc,iopt,FNMCK,LUMCK)
         Call WrMck(iRC,iOpt,'nSym',1,nBas,iDummer)
         Call ClsFls_MCLR()
         Call Finish(0)
      End if
C
*     iDisk=iToc(9)
*     IF(IPT2.EQ.0) iDisk=iToc(2)
*     Call dDaFile(LuJob,2,Work(ipCMO),ntBsqr,iDisk)
      If( .false. ) then
         jpCMO=ipCMO
         Do 15 iSym=1,nSym
            call dcopy_(nbas(isym)*ndel(isym),0d0,0,
     *                 Work(jpCMO+norb(isym)*nbas(isym)),1)
            Write(Line,'(A,i2.2)') 'MO coefficients, iSym = ',iSym
            Call RecPrt(Line,' ',Work(jpCMO),nBas(iSym),nBas(iSym))
            jpCMO=jpCMO+nBas(iSym)*nBas(iSym)
15       Continue
      End If

*----------------------------------------------------------------------*
*     Load the CI vectors for the SA roots                             *
*----------------------------------------------------------------------*

! If doRGLR, introducing CI coeffieients later :
!    1) only coefficients of importants DETs using MPS2CI
!    2) and together with DET numbers from GUGA generation part
! doRGLR_cireconstruct for full reconstruction of CI coefficients
#ifdef _DMRG_
      if(.not.(doRGLR_cireconstruct.or.doRGLR_MPS))then
#endif
        Call GetMem('OCIvec','Allo','Real',ipCI,nConf*nroots)
        Do i=1,nroots
          j=iroot(i)
          iDisk=iToc(4)
          Do k=1,j-1
            Call dDaFile(LuJob,0,rdum,nConf,iDisk)
          End Do
          Call dDaFile(LuJob,2,Work(ipCI+(i-1)*nconf),nConf,iDisk)
        End Do
#ifdef _DEBUG_
        Do i=0,nroots-1            !yma
          inum=0
          dv_ci2=0.0d0
          do j=1,nconf
            if(abs(Work(ipCI+nconf*i+j-1)).lt.0.0d0)then !yma CI-threshold
              inum=inum+1
              Work(ipCI+nconf*i+j-1)=0.0d0
            else
              dv_ci2=dv_ci2+Work(ipCI+nconf*i+j-1)**2
            end if
          end do
        End DO
#endif
#ifdef _DMRG_
      else if (doRGLR_MPS) then
        if (nroots.gt.0) then


          ! Get the number of MPS parameters (incl. nonredundant)
          nconf_red = dmrg_import_civec_phase1(qcm_group_names(1)
     &                                     %states(iroot(1)),iroot(1)-1)

          ! Allocate memory for redundant MPS parameters
          Call GetMem('CIred','ALLO','Real',ipCIred,nconf_red)
          call dmrg_import_civec_phase2(iroot(1)-1,
     &                                  nconf_red,Work(ipCIred))

          ! Remove redundant MPS parameters from the MPS vector
          call mpsinfo_init(Work(ipCIred),nconf_red)
          nconf = mpsinfo%n_nonred

          ! Allocate the MPS parameter array for all states with
          ! the number of nonredundant MPS parameters as the dimension
          Call GetMem('OCIvec','ALLO','Real',ipCI,nconf*nroots)

          call mps_remove_redundant(Work(ipCIred),Work(ipCI))
          ! Ignore symmetry
          ncsf(1) = nconf

          do ist = 2, nroots
            itmplen = dmrg_import_civec_phase1(qcm_group_names(1)
     &                                 %states(iroot(ist)),iroot(ist)-1)
            if (itmplen.ne.nconf_red) then
              write(6,*)'** Error: Number of MPS parameters in state'//
     &                  ' 1 and ', iroot(ist), ' do not match!'
              call Quit(_RC_INTERNAL_ERROR_)
            end if
            call dmrg_import_civec_phase2(iroot(ist)-1,nconf_red,
     &                                    Work(ipCIred))
            call mps_remove_redundant(Work(ipCIred),
     &                                Work(ipCI+(ist-1)*nconf))

          end do

          Call GetMem('OCIvec','FREE','Real',ipCIred,nconf_red)
        else
          write(6,*)'** Error: Number of states is 0!**'
          call Quit_OnUserError()
        end if
      End If
#endif
*----------------------------------------------------------------------*
*     Load state energy                                                *
*----------------------------------------------------------------------*
      Call GetMem('Temp2','Allo','Real',ipTmp2,mxRoot*mxIter)
      iDisk=iToc(6)
#ifdef _DEBUG_
      If (debug) Then
         Write(6,*) 'NROOTS: ',nroots
         Write(6,*) 'iROOTS: ',(iroot(i),i=1,nroots)
         Write(6,*) 'lROOTS: ',lroots
      End If
#endif
      Call dDaFile(LuJob,2,Work(ipTmp2),mxRoot*mxIter,iDisk)

      Do  iter=0,mxIter-1
        Do i=1,nroots
          j=iroot(i)
          ! It should be 0.0d0 in DMRG case
          Temp=Work(ipTmp2+iter*mxRoot+j-1)
          If ( Temp.ne.0.0D0 ) ERASSCF(i)=Temp
*          If (debug) Write(*,*) ERASSCF(i),i
         End Do
      End Do

#ifdef _DEBUG_
      If (debug) Then
          Write(6,*) (Work(ipTmp2+i),i=0,lroots)
          Write(6,*)'RASSCF energies=',(ERASSCF(i),i=1,nroots)
      End If
#endif
      Call GetMem('Temp2','Free','Real',ipTmp2,mxRoot*100)
*
      nAct  = 0    ! 1/2

      nAct2 = 0
      nAct4 = 0
      Do iSym = 1, nSym
         nAct = nAct + nAsh(iSym)
        nAct2=nAct2+nAsh(iSym)**2
      End Do
      Do iS = 1, nSym
         Do jS = 1, nSym
            Do kS = 1, nSym
              lS=iEOr(iEOr(is-1,js-1),ks-1)+1
              nAct4=nAct4+nAsh(iS)*nAsh(jS)*nAsh(kS)*nAsh(lS)
            End Do
         End Do
      End Do

*     Call GetMem(' G1sq','Allo','Real',ipG1,nAct2)

      nG1 = nAct*(nAct+1)/2
      Call GetMem(' G1 ','Allo','Real',ipG1t,nG1)
      nG2=nG1*(nG1+1)/2
      Call GetMem(' G2 ','Allo','Real',ipG2t,nG2)

      call init_rdms(nroots,nact)
      Call RDDENS(Work(ipG1t),Work(ipG2t))
      ipg1=ipg1t
      ipG2=ipG2t
      ipg2tmm=ipg2
      ipg2tpp=ipg2

#ifdef _DEBUG_
      Call Triprt('G1',' ',Work(ipG1t),ntash)
      Call Triprt('G2',' ',Work(ipG2),ng1)
#endif

      nG1 = nAct*(nAct+1)/2
      nG2=nG1*(nG1+1)/2

!       Call Triprt('G1',' ',Work(ipG1t),ntash)  ! yma
!       Call Triprt('G2',' ',Work(ipG2),ng1)

*                                                                      *
************************************************************************
*                                                                      *
      Return
      End
