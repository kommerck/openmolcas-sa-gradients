************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
************************************************************************
      Subroutine Kap_CI(iph1,iph2,ips1)
#ifdef _DMRG_
      use dmrg_mini_interface
      use qcmaquis_info
      use qcmaquis_interface_wrapper
      use qcmaquis_interface_measurements
      use rdms, only : nD1, itri
#endif
      Implicit Real*8(a-h,o-z)

#include "Input.fh"
#include "WrkSpc.fh"
#include "Pointers.fh"
#ifdef _DMRG_
#include "dmrginfo_mclr.fh"
#include "stdalloc.fh"
      real*8, allocatable :: mps_red(:)
      real*8, allocatable :: act_fock(:)
#endif
      Real*8 R(0:mxroot-1,0:mxroot-1)

#ifdef _DMRG_
!       if (.true.) then !!! disable alternative DMRG kap_ci
      if (.not.doRGLR_MPS) then !!! enable alternative DMRG kap_ci for debugging
#endif
        Call CISigma_sa(0,state_sym,state_sym,iph1,iph2,
     &                    idum,ipCI,ips1,'N')

#ifdef _DMRG_
      else !!! Alternative DMRG kap_ci
           !!! This is the one that works for now, the other one doesn't
      !!! Build the sigma vector including the core contribution
      !!! with the same transformed integrals as for CASSCF case

      !!! There's no proper equation for this neither in Stalring Mol.Phys.2001,99, 103-114
      !!! nor in Bernhardsson Mol. Phys. 1999, 96, 617-628
      !!! but I assume it's close enough to Eq. 50 in Snyder, except that the core contribution
      !!! (4alpha.C_I) is included in the modified sigma vector build

      ! Dump the transformed integrals into FCIDUMP
      ! For FCIDUMP, we need only the active block of the Fock matrix
      ! so copy it over from Work(iph1)
        call mma_allocate(act_fock, nD1)
        ! no symmetry support
        do i = 1, nash(1)
          do j = 1, i
            ii = nish(1)+i
            jj = nish(1)+j
            act_fock(itri(i,j)) = Work(iph1+(ii-1)+(jj-1)*norb(1))
          end do
        end do
        corenergy = 0.0d0
        call dmrg_interface_ctl(
     &                          task   = 'fci dump',
     &                          x1     = act_fock,
     &                          x2     = Work(iph2),
     &                          energy = corenergy,
     &                    checkpoint1  = 'FCIDUMP_mod'
     &                         )
        call mma_allocate(mps_red, mpsinfo%n_red)
        do i=0,nroots-1
          call dmrg_import_sigmavec(
     &             qcm_group_names(1)%states(iroot(i+1)),
     &             'FCIDUMP_mod',mps_red,mpsinfo%n_red)
          call mps_remove_redundant(mps_red,
     &          Work(ipin(ips1)+i*ncsf(state_sym)))
          call DSCAL_(ncsf(state_sym),Weight(i+1),
     &            Work(ipin(ips1)+i*ncsf(state_sym)),1)
        end do
        call mma_deallocate(mps_red)
        call mma_deallocate(act_fock)

      end if
#endif

      Call DSCAL_(nroots*ncsf(STATE_SYM),2.0d0,
     &           Work(ipin(ips1)),1)
!       write(6,*)" ipin(ips1)=", ipin(ips1)

      Do i=0,nroots-1
      Do j=0,nroots-1
        R(i,j)=ddot_(nconf1,Work(ipin(ips1)+nconf1*i),1,
     &                     Work(ipin(ipci)+nconf1*j),1)
      End DO
      End Do

      Do i=0,nroots-1
      Do j=0,nroots-1
      call daxpy_(nconf1,-R(i,j),
     &                  Work(ipin(ipci)+i*nconf1),1,
     &                  Work(ipin(ipS1)+j*nconf1),1)
      End DO
      End DO

      Return
      End
