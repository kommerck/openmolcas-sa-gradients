************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 1996, Anders Bernhardsson                              *
************************************************************************
      SubRoutine Precabb_2(ib,is,js,nd,nba,no,rout,Temp1,ntemp,
     &                   Scr,Temp2,fockti,
     &                   focki,focka,fock,sign)
************************************************************************
*                                         [2]
*   Calculates the diagonal submatrix of E    that couple
*
*   kappa           with   kappa                for a
*        kactive,virtual        kactive,virtual
*
*   single active index.
*   Used for preconditioner.
*
*   See Olsen,Yeager, Joergensen:
*    "Optimization and characterization of an MCSCF state"
*
*   Called by prec
*
*   ib,is       :       active index for the submatrix
*   js          :       symmetry of virtual,virtual
*   rOut        :       Submatrix
*
************************************************************************
      Implicit Real*8(a-h,o-z)
#include "Input.fh"
#include "Pointers.fh"
#include "WrkSpc.fh"
      Real*8 rout(*)
      Real*8 Focki(no,no),Focka(no,no),
     &       Fock(no,no)
      Real*8 Temp1(nTemp),Temp2(nO,nO), Scr(nTemp)
*                                                                      *
************************************************************************
*                                                                      *
      iTri(i,j)=Max(i,j)*(Max(i,j)-1)/2+Min(i,j)
      iTri1(i,j)=nTri-itri(nd-Min(i,j)+1,nd-Min(i,j)+1)
     &          +Max(i,j)-Min(i,j)+1
*                                                                      *
************************************************************************
*                                                                      *
      nTri=itri(nd,nd)
*
      iib=ib+nA(is)
      jVert=nOrb(js)-nAsh(js)-nIsh(js)
      If (jvert.eq.0) Return
*
      i2=nD-jVert+1
      ip=iTri1(i2,i2)
      rF=sign*Fockti
      call dcopy_(nBa**2,0.0d0,0,Temp2,1)
*

!      ij=0  ! yma
!      write(6,*)" ==> The packed 2-RDMs <== " 
!      do i=1,6  ! nact*(nact+1)/2
!        do j=1,i 
!          ij=ij+1
!          write(6,"(f12.8)",advance='no')work(ipG2+ij-1)
!        end do
!        write(6,*)
!      end do

      Do kS=1,nSym
        iOpt=1
        ijB=1
        ijBas=0
        ijBB=0
        If (nOrb(js)*nash(ks).gt.0) Then

        Do kBB=nish(ks)+1,nB(kS)
         Do kCC=nish(ks)+1,kBB
              Call COUL(jS,jS,kS,kS,kbb,kcc,Temp1,Scr)
              ipT=1

         If (kBB.gt.nish(ks).and.kCC.gt.nish(ks)) Then
           kkB=kBB+nA(ks)-nish(ks)
           kkC=kCC+nA(ks)-Nish(ks)
           rDens1=sign*2.0d0*Work(ipG2-1+
     &            itri(itri(iib,iib),itri(kkb,kkc)))
*
!           write(6,*)"itri(itri(iib,iib),itri(kkb,kkc))",
!     &                itri(itri(iib,iib),itri(kkb,kkc))  ! yma
!           write(6,*)"kbb,kcc",kbb,kcc, " rdens1 (rdm2)  ", rdens1   ! yma
!           call flush(6)

           If (kbb.ne.kcc) rdens1=rdens1*2.0d0

           Call DaxPy_(nO**2,rdens1,Temp1,1,Temp2,1)

!           write(6,*)" rdens1 if * 2 ", rdens1   ! yma
!           call flush(6)
          
!           ij=0 
!           write(6,*)" ----> Debugging print for Temp1[ij] <---- " ! yma
!           do i=1,nO
!             do j=1,nO
!               ij=ij+1
!               write(6,"(f12.8)",advance='no') Temp1(ij)
!             end do
!             write(6,*)
!           end do 

!           write(6,*)" ----> Debugging print for Temp2[i,j] <---- "
!           do i=1,nO     ! yma
!             do j=1,nO
!               write(6,"(f12.8)",advance='no')temp2(i,j)
!             end do
!             write(6,*)           
!             call flush(6)
!           end do 
!           call flush(6)

         End If
        End Do
       End Do
       End If
      End Do

!      write(6,*)" --> Debugging print for Temp2[i,j] after gt gt <-- "
!      do i=1,nO     ! yma
!        do j=1,nO
!          write(6,"(f12.8)",advance='no')temp2(i,j)
!        end do
!        write(6,*)           
!        call flush(6)
!      end do 
!      call flush(6)


*
      Do Ks=1,nsym
       iOpt=1
       JLB=1
       JLBas=0
       ijkl=nOrb(js)*nash(ks)
       If (ijkl.ne.0) Then
*
        jlBB=0
        Do LB=nish(ks)+1,nB(KS)
         kkc=nA(ks)+lb-nish(ks)
         Do JB=nish(ks)+1,nB(KS)
          kkb=nA(ks)+jb-nish(ks)
          Call EXCH(js,ks,js,ks,jb,lb,Temp1,Scr)
          ipT=1
          If (LB.gt.nISH(ks).and.jb.gt.nish(ks)) Then
           rDens2=sign*4.0d0*Work(ipG2-1+
     &         itri(itri(iib,kkc),itri(kkb,iib)))

!           write(6,*)"LB,JB",LB,JB," rdens2 ", rdens2   ! yma
!           call flush(6)

!           write(6,*)"itri(itri(iib,kkc),itri(kkb,iib))",
!     &                itri(itri(iib,kkc),itri(kkb,iib))  ! yma
!           write(6,*)"iib,kcc",iib,kcc, " rdens1 (rdm2)  ", rdens2   ! yma
!           call flush(6)

!           ij=0
!           write(6,*)" -2nd-> Debugging print for Temp1[ij] <-2nd- " ! yma
!           do i=1,nO
!             do j=1,nO
!               ij=ij+1
!               write(6,"(f12.8)",advance='no') Temp1(ij)
!             end do
!             write(6,*)
!           end do

!           write(6,*)" -2nd-> Debugging print for Temp2[i,j] <-2nd- "
!           do i=1,nO     ! yma
!             do j=1,nO
!               write(6,"(f12.8)",advance='no')temp2(i,j)
!             end do
!             write(6,*)
!             call flush(6)
!           end do 
!           call flush(6)

           Call DaXpY_(nO**2,rDens2,Temp1(ipT),1,Temp2,1)

!           ij=0
!           write(6,*)" -2nd2-> Debugging print for Temp1[ij] <-2nd2- "
!           do i=1,nO
!             do j=1,nO
!               ij=ij+1
!               write(6,"(f12.8)",advance='no') Temp1(ij)
!             end do
!             write(6,*)
!           end do

!           write(6,*)" -2nd2-> Debugging print for Temp2[i,j] <-2nd2- "
!           do i=1,nO     ! yma
!             do j=1,nO
!               write(6,"(f12.8)",advance='no')temp2(i,j)
!             end do
!             write(6,*)
!             call flush(6)
!           end do
!           call flush(6)

          End If
         End Do
        End Do
       End If
      End Do

!      write(6,*)" -2nd gt-> Debugging print for Temp2[i,j] <-2nd2 gt- "
!      do i=1,nO     ! yma
!        do j=1,nO
!          write(6,"(f12.8)",advance='no')temp2(i,j)
!        end do
!        write(6,*)
!      end do
!      call flush(6)

      iu=ip

      rho=sign*2.0d0*Work(ipg1-1+itri(iib,iib))

!      write(6,*)"rho",rho  ! yma
 
      Do iI=nAsh(js)+nIsh(js)+1,nOrb(js)

!       write(6,*)"The outside iI loop : "  ! yma
!       write(6,*)"js", js, "nAsh(js)",nAsh(js), 
!     &           "nIsh(js)",nIsh(js),"nOrb(js)",nOrb(js)

!       write(6,"(A,f12.8,A,I8,A,f12.8,A,f12.8,A,f12.8,A,I8,A,f12.8)")
!     &           " rho ",rho," ip ",ip," rout(ip) ",
!     &           rout(ip)," rf ", rf, " FockI(iI,ii) ",FockI(iI,ii),
!     &           " ii ",ii, " Temp2(ii,ii) ",Temp2(ii,ii)  ! yma

       rOut(ip)=rout(ip)-2.0d0*rF+Rho*FockI(iI,ii)+Temp2(ii,ii)
!       write(6,*)" rout(ip) ",rout(ip)  ! yma
       ip=ip+1
       Do iJ=iI+1,NOrb(js)

!         write(6,*)"The inside ij loop : "  ! yma
!         write(6,
!     &     "(A,f12.8,A,I8,A,f12.8,A,f12.8,A,f12.8,A,I8,A,I8,A,f12.8)")
!     &           " rho ",rho," ip ",ip," rout(ip) ",
!     &           rout(ip)," rf ", rf, " FockI(iI,ij) ",FockI(iI,iJ),
!     &           " ii ",ii, " ij ", ij,  " Temp2(ii,ij) ",Temp2(ii,ij)  ! yma 

         rOut(ip)=Rho*FockI(iI,iJ)+Temp2(ii,ij)

!         write(6,*)" rout(ip) ",rout(ip)  ! yma 
 
         ip=ip+1
       End Do
      End Do

!      write(6,*) "Before finishing Precabb_2"  ! yma
!      Do i=1,min(NOrb(js),10)
!        write(6,'(10F12.8)') (rout((j-1)*(2*nd-j+2)/2+i-j+1),
!     &                       j=1,i)
!      End Do

      return
c Avoid unused argument warnings
      If (.False.) Then
       Call Unused_real_array(focka)
       Call Unused_real_array(fock)
      End If
      end

