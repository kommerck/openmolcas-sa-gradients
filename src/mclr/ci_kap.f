************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
************************************************************************
*
*    Construct the Hoc . C part of the sigma vector
*    See Snyder et al. JCP (2017) 146, 174113, Eq. 41 (or 40)
      Subroutine CI_KAP(ipcid,fock,fockOut,isym)
      use fockmatrices
#ifdef _DMRG_
      use dmrg_mini_interface
      use qcmaquis_interface_measurements
      use rdms, only: nD1, nD2, unfold_rdms
!       use tmat ! for the 2nd term in Hoc.c in Eq. 40
#endif
      Implicit Real*8(a-h,o-z)

#include "Input.fh"
#include "Pointers.fh"
#include "WrkSpc.fh"
#include "dmrginfo_mclr.fh"
#include "stdalloc.fh"

      Real*8 Fock(*),FockOut(*)

#ifdef _DMRG_
! needed for the 2nd term in Hoc.c in Eq. 40
! Uncompressed T matrix for a specific state
!       real*8,allocatable::Tss(:)
#endif
      irc=ipnout(-1)
      Call Getmem('De','allo','real',ipde,ntash**2)
      Call Getmem('P','allo','real',ipp,n2dens)
***** Eq. 41 states we need to build the T matrix for the densities calculated
*     with the updated CI coefficients
*     T matrix is built with fockgen, so here we obtain the densities
*     For DMRG, we use dmrg_get_average_lagrangeRDMs to obtain the average densities
*     For CI, we use CIDens_SA

#ifdef _DMRG_
      if (doRGLR_MPS) then
        ! we ignore symmetry as usual

        call dmrg_get_average_lagrangeRDMs(Work(ipin(ipCId)),
     &     Work(ipde),Work(ipP))
        irc=opout(ipCId)

      else
#endif
      Call CIDens_SA(.true.,ipCI,ipCid,State_sym,State_Sym,
     &                Work(ipP),Work(ipde))
#ifdef _DMRG_
      end if
#endif

!       if (.not.doRGLR_MPS) then
!         call projecter(Work(ipin(ipCID)),Work(ipin(ipci)),
!      &     Work(ipDe),Work(ipp))
!       end if
        call dcopy_(ndens2,0.0d0,0,Fock,1)
        call dcopy_(ndens2,0.0d0,0,FockOut,1)
        d0=0.0d0
*       Now build the T matrix using the densities obtained above
*       TODO: Probably adding .false. as a last parameter would speed
*       the call below a bit, but needs some debugging

        Call FockGen(d0,Work(ipDe),Work(ipP),Fock,FockOut,isym)

#ifdef _DMRG_
!!!! Last term in Eq. 40 in Snyder et al.
!!!! For CI this is handled by the Projecter() call above, but it's not required
!!!! Nevertheless here's its DMRG implementation
!!!! Uses the T matrix which is disabled for now, so when the code below is enabled,
!!!! the T matrix construction must be enabled too in start_mclr.f
!         if (doRGLR_MPS) then
!           !! FockOut_pq -= sum_Y T_pq (Psi,Y) . sum_I c_I^Y ctilde_PsiI
!
!           call mma_allocate(Tss, ndens2)
!           Tss = 0.0d0
!           do i = 0, nroots-1
!           do j = 0, nroots-1
!             ! first calculate sum_I c_I^Y ctilde_PsiI
!             alpha = ddot_(ncsf(State_Sym),
!      &        Work(ipin(ipCI)+j*ncsf(state_sym)),1,
!      &        Work(ipin(ipCId)+i*ncsf(state_sym)),1)
!             call Uncompress(T(itri(i,j),:),Tss,isym)
!             ! Multiply it with T(Psi,Y)
!             call daxpy_(ndens2,alpha,Tss,1,FockOut,1)
!           end do
!           end do
!           call mma_deallocate(Tss)
!         end if
#endif
      Call Getmem('De','free','real',ipde,ntash**2)
      Call Getmem('P','free','real',ipp,n2dens)
      Call Getmem('P','chec','real',ipp,n2dens)


      return
      end

#ifdef _NOTUSED_
      Subroutine Projecter(CID,CI,D,P)
      Implicit Real*8(a-h,o-z)

#include "Input.fh"
#include "Pointers.fh"
#include "WrkSpc.fh"
      Real*8 CI(*),CID(*),P(*),D(*)
      Call Getmem('DeDe','allo','real',ipdex,n1dens)
      Call Getmem('PP','allo','real',ippx,n2dens)
      Do i=0,nroots-1
       Do j=0,nroots-1
        r=ddot_(nconf1,ci(1+nconf1*i),1,
     &                   cid(1+nconf1*j),1)
        Call CIDens(.true.,ci(1+nconf1*i),
     &               ci(1+nconf1*j),State_sym,State_sym,1,
     *     work(ippx),work(ipdex))
       call daxpy_(ntash**2,-r*weight(1+i),work(ipdex),1,d,1)
       call daxpy_(n2dens,-r*weight(1+i),work(ippx),1,p,1)
       End Do
      End Do
      Call Getmem('DeDe','free','real',ipdex,n1dens)
      Call Getmem('PP','free','real',ippx,n2dens)
      Return
      End
#endif
