************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
************************************************************************
* Copyright (C) 2018 Leon Freitag                                      *
*                                                                      *
* This file implements routines to construct the T matrix required to  *
* construct the sigma vectors from                                     *
* orbital-CI and CI-CI Hessian blocks, i.e.                            *
* Hoc x kappa, Hco x C, Hcc x                                          *
* from a DMRG wavefunction. Necessary quantities are obtained from     *
* an additional QCMaquis DMRG run.                                     *
* These routines follow Snyder et al. JCP (2017) 146, 174113,          *
* where the CASSCF version of these routines has been published.       *
************************************************************************
      module tmat
      implicit none

      real*8, allocatable :: T(:,:)

      contains
      ! allocation and deallocation routines
      subroutine init_Tmat
        use rdms
#include "Pointers.fh"
#include "Input.fh"
#include "stdalloc.fh"
        call mma_allocate(T, nrootpair, ndens2)
      end subroutine init_Tmat

      subroutine dealloc_Tmat
#include "stdalloc.fh"
        if (allocated(T)) call mma_deallocate(T)
      end subroutine dealloc_Tmat

!!! Construct the T matrix for all state pairs I and J
! * The T matrix is defined in Eqs. 35-37
! * and has the dimensions of (nstate*(nstate+1)/2,norb*(norb+1)/2)
! * in triangular format

!!! Here we use the old orbital gradient routines to construct the T matrix
!!! It's storage inefficient because we have to store the large virtual-
!!! virtual block, so in the future we should get rid of it
      subroutine buildTmat
      use fockmatrices
      implicit none
#include "Pointers.fh"
#include "Input.fh"
#include "stdalloc.fh"
      ! temporary matrices for RDMs in square form
      ! and the fock matrix produced by fockgen
      real*8, allocatable :: rdm1sq(:), rdm2sq(:), tempfock(:)

      integer i,j

      call mma_allocate(rdm1sq, ntash**2)
      rdm1sq = 0.
      call mma_allocate(rdm2sq, itri(ntash**2,ntash**2))
      rdm2sq = 0.
      call mma_allocate(tempfock, ndens2)
      tempfock = 0.

      do i = 1, nroot
      do j = 1, i
        call unfold_rdms(D1(itri(i,j),:), D2(itri(i,j),:),
     &                   rdm1sq,rdm2sq)
        call fockgen(1.0d0,rdm1sq,rdm2sq,tempfock,T(itri(i,j),:),
     &               1,i.ne.j)
      end do
      end do

      if (allocated(rdm1sq)) call mma_deallocate(rdm1sq)
      if (allocated(rdm2sq)) call mma_deallocate(rdm2sq)
      if (allocated(tempfock)) call mma_deallocate(tempfock)
      end subroutine buildTmat

!!!!!! Alternative building routine of the T matrix, which closely follows the paper.
!!!!!! Doesn't work yet !!!!!
! * Construct the T matrix for all state pairs I and J
! * The T matrix is defined in Eqs. 35-37
! * and has the dimensions of (nstate*(nstate+1)/2,norb*(norb+1)/2)
! * in triangular format
! * i.e. to be addressed
! *
! * Output: blocks of T: Tit, Tia, Tta
! * full T matrix  (for all states, Psi, Phi) may be reconstructed as follows:
! * *    i   t   a
! *  +-----+-----+---+
! * i|0?   | Tit |Tia|
! *  +-----+-----+---+
! * t|-Tit?|  0? |Tta|
! *  +-----+-----+---+
! * a|-Tia?|-Tta?|0? |
! *  +-----+-----+---+
! *
! ** Essentially the T matrix is the generalised orbital gradient for two
! * different states (compare Eq.35-37 to Eq. 13-15).
! * TODO: Once this works, replace the explicit calculation of the orbital gradient
! * (in rhs.f?) by reading the T matrix
! * TODO:! Symmetry is not supported: volunteers are welcome to code symmetry support
! * Necessary density matrix elements and integrals are fetched
!       subroutine buildTmat(Tit,Tia,Tta)
!
!       use rdms
!       use fockmatrices
!       implicit none
! #include "Pointers.fh"
! #include "WrkSpc.fh"
! #include "Input.fh"
! #include "stdalloc.fh"
!       real*8, intent(out) :: Tit(nrootpair,ndens2),
!      &   Tia(nrootpair,ndens2), Tta(nrootpair,ndens2)
!
!       ! Array for the temporary Q matrices
!       real*8, allocatable :: QTemp(:)
!       ! and two scratch arrays for CreQ2()
!       real*8, allocatable :: tmp1(:),tmp2(:)
!
!       integer :: i,t,u,a,ist
!       integer :: aa, tt, uu
!
!       ! ditch the symmetry support
!       ! add up all orbital numbers from all symmetries
!
!       integer :: nish_, nash_, nssh_
!       nish_ = sum(nish)
!       nash_ = sum(nash)
!       ! frozen and deleted orbitals are ignored
!       nssh_ = sum(nBas) - nish_ - nash_
!
!       ! allocate temporary arrays
!       call mma_allocate(QTemp, nDens2)
!       QTemp = 0.
!       call mma_allocate(tmp1, nDens2)
!       tmp1 = 0.
!       call mma_allocate(tmp2, nDens2)
!       tmp2 = 0.
!
!       ! T_ia: inactive-virtual block
!       ! Eq. 35
!       Tia = 0.
!       do i = 1, nish_
!         do a = 1, nssh_
!           aa = nish_ + nash_+ a
!
!           !              IJ              core
!           ! Fcore part: T   = 4 delta   F
!           !              ia          IJ  ia
!           !
!           ! Fcore is read from FI (fockmatrices module)
!
!           do ist = 1, nroot
!             Tia(itri(ist,ist),itri(i,aa)) = 4.d0*FI(itri(i,aa))
!           end do
!
!           ! Eq. 35 continued
!           ! (mind the factor of 2 in front of Fact)
!           !
!           !                  IJ     act(IJ)
!           !      Fact part: T  += 4F
!           !                  ia     ia
!
!           Tia(:,itri(i,aa)) = Tia(:,itri(i,aa)) +
!      &        4.d0*FAT(:,itri(i,aa))
!         end do
!       end do
!
!       ! T_ta: active-virtual block. Eq. 36
!       Tta = 0.
!
!       do t = 1, nash_
!         do a = 1, nssh_
!           tt = nish_ + t
!           aa = nish_ + nash_ + a
!
!           !              IJ         IJ  core
!           ! Fcore part: T  = 2sum d1   F
!           !              ta    u    tu  au
!
!           ! sum over u: note,we don't use DGEMM for
!           ! one-index transformation due to triangular form
!
!           do u = 1,nash_
!             uu = nish_ + u
!             Tta(:,itri(tt,aa)) = Tta(:,itri(tt,aa)) +
!      &           2*D1(:,itri(t,u))*FI(itri(aa,uu))
!           end do
!
!           !             IJ              IJ
!           ! Fact part: T   = 2 sum    D2     (au|vw)
!           !             ta       uvw    tuvw
!           !
!           ! (Mind the factor of 2 compared to Eq.36)
!           !
!           ! This transformation is identical to the creation of
!           ! the Q matrices (in creq.f, creq2.f, creqadd.f etc.)
!           ! see Eq. 32 in Bernhardsson et al.
!           ! Mol. Phys. 1999, 96, 617-628, so we will just call
!           ! the appropriate routine
!
!           do ist = 1, nrootpair
!             Call CreQ2(QTemp,D2(ist,:),1,tmp1,tmp2,nD2)
!           ! QTemp should now contain
!           !           IJ
!           ! sum     D2     (au|vw)
!           !    uvw    tuvw
!             Tta(ist,itri(tt,aa)) =
!      &          Tta(ist,itri(tt,aa)) + 2.d0*QTemp(itri(tt,aa))
!           end do
!         end do
!       end do
!
!       ! T_it: inactive-active block. Eq. 37
!       Tit = 0.
!
!       ! First two parts are equivalent to the inactive-virtual
!       ! part (see above and Eq. 35), just with different indices.
!       do i = 1, nish_
!         do t = 1, nash_
!           tt = nish_ + t
!
!           do ist = 1, nroot
!             Tit(itri(ist,ist),itri(i,tt)) = 4.d0*FI(itri(i,tt))
!           end do
!
!           Tit(:,itri(i,tt)) = Tit(:,itri(i,tt)) +
!      &        4.d0*FAT(:,itri(i,tt))
!
!           ! Now subtract the last two terms in Eq. 37
!           ! These are equivalent to the terms in the active-virtual
!           ! block (Eq. 36) but again with different indices
!
!           do u = 1,nash_
!             uu = nish_ + u
!             Tit(:,itri(i,tt)) = Tit(:,itri(i,tt)) -
!      &           2.d0*D1(:,itri(t,u))*FI(itri(i,uu))
!           end do
!
!           do ist = 1, nrootpair
!             Call CreQ2(QTemp,D2(ist,:),1,tmp1,tmp2,nD2)
!             Tit(ist,itri(i,tt)) =
!      &          Tit(ist,itri(i,tt)) - 2.d0*QTemp(itri(i,tt))
!           end do
!         end do
!       end do
!
!       if (allocated(QTemp)) call mma_deallocate(QTemp)
!       if (allocated(tmp1)) call mma_deallocate(tmp1)
!       if (allocated(tmp2)) call mma_deallocate(tmp2)
!
!       end subroutine buildTmat

      end module tmat
