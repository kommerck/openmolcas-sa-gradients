************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2018, Leon Freitag                                     *
************************************************************************
*                                                                      *
* This file contains interfaces to QCMaquis routines required in DMRGSA*
* gradient calculations that have not made it yet into the mainstream  *
* branch (and are found in the linear-response-clean branch). These    *
* require a call to a special QCMaquis build (of the aforementioned    *
* branch). It is assumed that the dmrg_meas executable of this branch  *
* is found at $QCMAQUIS_LR_PATH/dmrg_meas.                             *
* This is still a large WIP, most of the routines that should belong   *
* here are found elsewhere.                                            *
************************************************************************
*
* If enabled, disables the removal of redundant parameters
#define _MPS_REDUNDANT_

      module dmrg_mini_interface
      implicit none
#ifdef _DMRG_
      ! Keeps track of redundant MPS parameters
      type mpsparinfo_type
        ! Number of nonredundant MPS parameters
        integer :: n_nonred
        ! Number of all MPS parameters
        integer :: n_red
        ! Indexes of all redundant MPS parameters
        integer, allocatable :: red_index(:)
      end type
      type(mpsparinfo_type), public :: mpsinfo

      save

      contains
      ! Initialisation routines
      ! Read in the number and the positions of redundant MPS parameters
      ! Input: red_mps: vector of MPS parameters (incl. redundant)
      !        n_red: length of red_mpsvec
      ! (we use assumed-size array because we work with F77 routines)
      subroutine mpsinfo_init(red_mps, n_red)
#include "stdalloc.fh"

        real*8, intent(in) :: red_mps(*)
        integer, intent(in) :: n_red

        integer :: i, idx
        real*8, parameter :: thresh = 1e-26

        mpsinfo%n_red = n_red

#ifndef _MPS_REDUNDANT_
        idx = 0
        do i = 1, n_red
          if (abs(red_mps(i)) < thresh) idx = idx + 1
        end do

        call mma_allocate(mpsinfo%red_index,idx)
        mpsinfo%n_nonred = n_red - idx

        idx = 1
        do i = 1, n_red
          if (abs(red_mps(i)) < thresh) then
            mpsinfo%red_index(idx) = i
            idx = idx + 1
          end if
        end do
#else
        mpsinfo%n_nonred = n_red
#endif
      end subroutine mpsinfo_init

      subroutine mpsinfo_deinit
#include "stdalloc.fh"
#ifndef _MPS_REDUNDANT_
        if (allocated(mpsinfo%red_index))
     &     call mma_deallocate(mpsinfo%red_index)
#endif
      end subroutine mpsinfo_deinit

      ! Remove redundant MPS parameters from an MPS parameter vector
      ! Input: mps_nonred: vector of MPS parameters (incl. redundant)
      ! Output:   mps_red: vector of nonredundant MPS parameters
      !                    with length mpsinfo%n_red
      subroutine mps_remove_redundant(mps_nonred,mps_red)
        real*8,intent(in)  :: mps_nonred(*)
        real*8,intent(out) :: mps_red(*)
#ifndef _MPS_REDUNDANT_
        integer :: i, idx

        idx = 1
        do i = 1, mpsinfo%n_red
          ! any is inefficient, replace it?
          if (.not.any(mpsinfo%red_index == i)) then
            mps_red(idx) = mps_nonred(i)
            idx = idx + 1
          end if
        end do
#else
        mps_red(1:mpsinfo%n_red) = mps_nonred(1:mpsinfo%n_red)
#endif
      end subroutine mps_remove_redundant

      ! Add redundant MPS parameters to an MPS parameter vector
      ! at the original positions (saved in mpsinfo)
      subroutine mps_add_redundant(mps_red,mps_nonred)
      ! Input:     mps_red: vector of nonredundant MPS parameters
      ! Output: mps_nonred: vector of MPS parameters incl. redundant
      !                     with length mpsinfo%n_nonred
        real*8,intent(in) :: mps_red(*)
        real*8,intent(out)  :: mps_nonred(*)
#ifndef _MPS_REDUNDANT_
        integer :: i, idx

        idx = 1
        do i = 1, mpsinfo%n_red
          if (any(mpsinfo%red_index == i)) then
            mps_nonred(i) = 0.0d0
          else
            mps_nonred(i) = mps_red(idx)
            idx = idx + 1
          end if
        end do
#else
        mps_nonred(1:mpsinfo%n_red) = mps_red(1:mpsinfo%n_red)
#endif
      end subroutine mps_add_redundant

      ! Obtain state-averaged lagrange RDMs for a given MPS parameter
      ! vector
      ! (Eq. 69 and 72 in Snyder et al.), also required for Eq. 41
      ! Note that the output is scaled by 2 since all equations require
      ! a prefactor of 2w. The multiplication by 2 is implicit
      ! in the RDM symmetrisation.
      ! Input: mpsvec: vector of nonredundant MPS parameters for
      !                   all states (dim: nroots*nconf)
      ! Output: dv, pv: symmetrised 1- and 2-RDM
      ! Note that the original CI subroutine that yields densities (densi2)
      ! does NOT yield symmetrised RDMs, unlike this one
      subroutine dmrg_get_average_lagrangeRDMs(mpsvec,dv,pv)

        use qcmaquis_interface_measurements
        use qcmaquis_info
        use rdms
#include "stdalloc.fh"
#include "Input.fh"
#include "Pointers.fh"
        real*8, target, intent(in)  :: mpsvec(*)
        real*8, intent(out) :: dv(n1dens), pv(n2dens)

        ! pointer to mpsvec for pointer arithmetics
c         real*8, pointer :: mpsvec_p(:)

c         ! for state-specific density matrices
c         real*8,allocatable :: Ds(:), Ps(:)

        ! MPS parameters with added redundant parameters
c         real*8,allocatable :: mps_red(:)


c         integer ist
c         call mma_allocate(Ds,n1dens)
c         call mma_allocate(Ps,n2dens)

c         Ds = 0.0d0
c         Ps = 0.0d0

        dv = 0.0d0
        pv = 0.0d0

        call dmrg_import_average_rdm_update(qcm_group_names(1)
     &                                   %states,
     &                                     mpsvec,
     &                                     mpsinfo%n_red,
     &                                     dv,
     &                                     pv)

c c         call mma_allocate(mps_red,mpsinfo%n_red)
c         do ist=1, nroots
c           ! assign a slice to mpsvec array corresponding to 1 root to mpsvec_p
c           mpsvec_p => mpsvec(1+mpsinfo%n_nonred*(ist-1):
c      &                         mpsinfo%n_nonred*ist)
c           ! add back the redundant MPS parameters
c           call mps_add_redundant(mpsvec_p,mps_red)
c           ! calculate updated RDMs with QCMaquis
c           ! dmrg_import_rdm_update now returns symmetrised TDMs
c           ! with correct dimensions of n1dens and n2dens
c           ! (unlike qcmaquis_interface_ctl('imp rdmT') which
c           ! returns symmetrised TDMs in triangular form !)
c           call dmrg_import_rdm_update(qcm_group_names(1)
c      &                                   %states(iroot(ist)),
c      &                                mps_red,
c      &                                mpsinfo%n_red,
c      &                                Ds,
c      &                                Ps)
c
c           ! average
c           dv = dv + weight(iroot(ist))*Ds
c           pv = pv + weight(iroot(ist))*Ps
c         end do
c
c         ! deallocate
c         call mma_deallocate(mps_red)
c         call mma_deallocate(Ds)
c         call mma_deallocate(Ps)
      end subroutine dmrg_get_average_lagrangeRDMs

!!! old version
c       subroutine dmrg_get_average_lagrangeRDMs(mpsvec,dv,pv)
c
c         use qcmaquis_interface_measurements
c         use qcmaquis_info
c         use rdms
c #include "stdalloc.fh"
c #include "Input.fh"
c #include "Pointers.fh"
c         real*8, target, intent(in)  :: mpsvec(*)
c         real*8, intent(out) :: dv(n1dens), pv(n2dens)
c
c         ! pointer to mpsvec for pointer arithmetics
c         real*8, pointer :: mpsvec_p(:)
c
c         ! for state-specific density matrices
c         real*8,allocatable :: Ds(:), Ps(:)
c
c         ! MPS parameters with added redundant parameters
c c         real*8,allocatable :: mps_red(:)
c
c
c         integer ist
c         call mma_allocate(Ds,n1dens)
c         call mma_allocate(Ps,n2dens)
c
c         Ds = 0.0d0
c         Ps = 0.0d0
c
c         dv = 0.0d0
c         pv = 0.0d0
c
c c         call dmrg_import_average_rdm_update(qcm_group_names(1)
c c      &                                   %states,
c c      &                                     mpsvec,
c c      &                                     mpsinfo%n_red,
c c      &                                     dv,
c c      &                                     pv)
c
c c         call mma_allocate(mps_red,mpsinfo%n_red)
c         do ist=1, nroots
c           ! assign a slice to mpsvec array corresponding to 1 root to mpsvec_p
c           mpsvec_p => mpsvec(1+mpsinfo%n_nonred*(ist-1):
c      &                         mpsinfo%n_nonred*ist)
c           ! add back the redundant MPS parameters
c c           call mps_add_redundant(mpsvec_p,mps_red)
c           ! calculate updated RDMs with QCMaquis
c           ! dmrg_import_rdm_update now returns symmetrised TDMs
c           ! with correct dimensions of n1dens and n2dens
c           ! (unlike qcmaquis_interface_ctl('imp rdmT') which
c           ! returns symmetrised TDMs in triangular form !)
c           call dmrg_import_rdm_update(qcm_group_names(1)
c      &                                   %states(iroot(ist)),
c      &                                mpsvec_p,
c      &                                mpsinfo%n_red,
c      &                                Ds,
c      &                                Ps)
c
c           ! average
c           dv = dv + weight(iroot(ist))*Ds
c           pv = pv + weight(iroot(ist))*Ps
c         end do
c
c         ! deallocate
c c         call mma_deallocate(mps_red)
c         call mma_deallocate(Ds)
c         call mma_deallocate(Ps)
c       end subroutine dmrg_get_average_lagrangeRDMs

      !!! Calculate Hco.o for DMRG according to Snyder et al., eq. 50
      !!! Input:  kappa: uncompressed orbital trial vector (dimension: ndens2)
      !!!            ci: MPS parameter vector (dimension: nconf*nroots)
      !!! Output: ciout: Hco.o (dimension: nconf*nroots)
      !!! Doesn't work yet !!!!!!!! Use kap_ci() instead
      !!! Therefore it has been commented
!       subroutine dmrg_kap_ci(kappa, ci, ciout)
!
!       use fockmatrices
!       use rdms, only : itri
!       use qcmaquis_interface_wrapper
!       use qcmaquis_info
!       use tmat
! #include "stdalloc.fh"
! #include "Pointers.fh"
! #include "Input.fh"
!
!       real*8, intent(in) :: kappa(ndens2), ci(nconf*nroots)
!       real*8, intent(out):: ciout(nconf*nroots)
!
!       ! quantities from eq. 48 and 49
!       real*8, allocatable :: Dtilde(:), Dbar(:)
!
!       ! matrix to hold F from Eq. 45 + Eq. 46
!       ! Ftilde: non-symmetrised, full dimensions (norb x norb)
!       ! Fsym: symmetrised (norb(norb-1)/2)
!       real*8, allocatable :: Ftilde(:), Fsym(:)
!
!       ! 2e transformed integrals from Eq. 47
!       real*8, allocatable :: ints(:)
!
!       ! sigma vector from Eq. 51, with and without redundant MPS parameters
!       real*8, allocatable :: sigmavec_red(:), sigmavec(:)
!
!       real*8 :: alpha, corenergy
!
!       real*8, external :: ddot_
!
!       real*8 :: R(nroots*(nroots+1)/2)
!
!       integer :: ist, jst, i, j
!
!       R = 0.0d0
!       call mma_allocate(Dtilde, ndens2)
!       call mma_allocate(Dbar, ndens2)
!       call mma_allocate(Ftilde, nash(1)**2)
!       call mma_allocate(Fsym, nD1)
!
!       call mma_allocate(ints, nD2)
!
!       call mma_allocate(sigmavec_red,mpsinfo%n_red)
!       sigmavec_red = 0.0d0
!       call mma_allocate(sigmavec,mpsinfo%n_nonred)
!       sigmavec = 0.0d0
!
!       ciout(1:nconf*nroots) = 0.0d0
!
!
!       !!!       ~     _
!       !!! Build D and D (Eqs. 48 and 49)
!
!       call makeDtilde(kappa, Dtilde)
!       call makeDbar(kappa, Dbar)
!
!       !!! Build alpha according to Eq. 44
!       !!!                            ~
!       !!! -> alpha = sum_pq fcore_pq Dpq
!       !!!
!
!       alpha = ddot_(ndens2, Dtilde, 1, FI, 1)
!
!       !!! First term in Eq. 50: ciout = 4.alpha.c + ...
!       call dcopy_(nconf*nroots, ci, 1, ciout, 1)
!       call dscal_(nconf*nroots, 4*alpha, ciout, 1)
!
!       !!! Construct a modified sigma build according to Eq. 52
!       !!!                    ~          _
!       !!! 1e-integrals: Fact(D) + Fcore(D) -> Ftilde
!       !!! -> build Fact(D): Eq. 45
!       call makeFactDtilde(Dtilde, Ftilde)
!
!       !!! -> build Fcore(Dbar): Eq. 46 and add it to Ftilde
!       call addFcoreDbar(Dbar, Ftilde)
!
!       !!! 2e-integrals: (tu|vw)m according to Eq. 47
!       !!! -> 1index-transform of 2e-integrals
!       call transform_tuvwm(Dbar, ints)
!       !!! 2e integrals must be multiplied by 4 as required in Eq. 52
!
!       !!! warning! due to discrepancy in Snyder's Eq. 36 and Eq. 4.3b in
!       !!! Chaban et al. Theor Chem Acc (1997) 97,88-95
!       !!! I suspect that the factor should be 2, not 4
!       ints = 2.0d0*ints
!
!       !!! symmetrise and compress Ftilde
!       !!! and multiply by 2 as required in Eq. 52
!       !!! (by omitting the factor of 0.5 needed for symmetrisation)
!       Fsym = 0.0d0
!       do i = 1, nash(1)
!         do j = 1, i
!           Fsym(itri(i,j)) =
!      &        (Ftilde(i+(j-1)*nash(1))+Ftilde(j+(i-1)*nash(1)))
!         end do
!       end do
!
!       !!! write the transformed 1e and 2e-integrals to FCIDUMP
!       ! corenergy = rin_ene+potnuc !!! which is correct?
!       corenergy = 0.0d0
!       call dmrg_interface_ctl(
!      &                          task   = 'fci dump',
!      &                          x1     = Fsym,
!      &                          x2     = ints,
!      &                          energy = corenergy,
!      &                    checkpoint1  = 'FCIDUMP_mod'
!      &                         )
!       do ist=1,nroots
!         !!! Launch QCMaquis with modified integrals, extract the sigma vector
!         call dmrg_import_sigmavec(
!      &             qcm_group_names(1)%states(iroot(ist)),
!      &             'FCIDUMP_mod',sigmavec_red,mpsinfo%n_red)
!         call mps_remove_redundant(sigmavec_red,sigmavec)
!
!         !!! add the sigma vector to the final result (2nd term in Eq. 50)
!         ciout((ist-1)*nconf+1:ist*nconf) =
!      &     ciout((ist-1)*nconf+1:ist*nconf) + sigmavec
!       end do
!         !!! Construct the last term in Eq. 50
!         !!! ciout -= sum_(pq,Y) C_Y kappa_pq T_pq(Psi,Y)
!       do ist=1,nroots
!         do jst=1,ist
!           R(itri(ist,jst))=ddot_(ndens2,kappa,1,T(itri(ist,jst),:),1)
!         end do
!       end do
!
!       do ist=1,nroots
!         do jst=1,nroots
!           ciout((ist-1)*nconf+1:ist*nconf) =
!      &       ciout((ist-1)*nconf+1:ist*nconf)
!      &     - ci((jst-1)*nconf+1:jst*nconf)
!      &     * R(itri(ist,jst))
!         end do
!       end do
!
!         !!! multiply ciout by 2*weight
!         ciout((ist-1)*nconf+1:ist*nconf) =
!      &       ciout((ist-1)*nconf+1:ist*nconf)*2.0d0*weight(ist)
!
!
!
!       call mma_deallocate(sigmavec)
!       call mma_deallocate(sigmavec_red)
!       call mma_deallocate(ints)
!       call mma_deallocate(Ftilde)
!       call mma_deallocate(Fsym)
!       call mma_deallocate(Dtilde)
!       call mma_deallocate(Dbar)
!
!       end subroutine dmrg_kap_ci
!
!       !!! helper routines used in dmrg_kap_ci
!       !!! calculate transformed integrals in eq. 47
!       !!! Input: D: Dbar in Eq. 47
!       !!! Output: intout: transformed integrals
!       subroutine transform_tuvwm(D, intout)
!
!       use rdms, only : itri, nD2
! #include "Input.fh"
! #include "Pointers.fh"
! #include "stdalloc.fh"
!
!       real*8, intent(in) :: D(ndens2)
!       real*8, intent(out) :: intout(nD2) ! (nact*(nact-1)/2)(nact*(nact-1)/2-1)/2
!
!       ! scratch for fetching of integral batches
!       real*8, allocatable :: ints(:), scr(:)
!
!       integer :: p, t, u, v, w, uu, tt, ipI, ipD, ipDT, ipOut
!
!       call mma_allocate(ints, ndens2)
!       call mma_allocate(scr, ndens2)
!
!       intout = 0.0d0
! *
!       ! no symmetry support
!       Do w=1,nAsh(1)
!         Do v=1,nAsh(1)
!           ! get an integral batch g_tuvw with indices (v,w)
!           ! Coul returns an norb x norb matrix g_tu^(v,w)
!           Call Coul(1,1,1,1,nIsh(1)+v,nIsh(1)+w,ints,Scr)
!           Do u=1,nAsh(1)
!             Do t=1,nAsh(1)
!               Do p = 1,nOrb(1)
!
!                 uu = nIsh(1) + u
!                 tt = nIsh(1) + t
!
!                 ! index for integrals
!                 ipI = p + (uu-1)*nOrb(1)
!
!                 ! index for D
!                 ipD = tt + (p-1)*nOrb(1)
!
!                 ! index for D^T
!                 ipDt = p + (tt-1)*nOrb(1)
!                 ! index for output
!                 ipOut = itri(itri(t,u),itri(v,w))
!
!                 ! symmetric transform
!                 intout(ipOut) = intout(ipOut) +
!      &                  .5d0*(D(ipD)+D(ipDT))*ints(ipI)
!               End Do
!             End Do
!           End Do
!         End Do
!       End Do
! *
!       call mma_deallocate(ints)
!       call mma_deallocate(scr)
!       end subroutine transform_tuvwm
!
!       !!!                            ~            _
!       !!! Routines to construct Fact(D) and Fcore(D) (Eqs. 45 and 46)
!       !!! No symmetry supported
!       !!!                ~
!       !!! Construct Fact(D)
!       !!! Input: Dtilde (norb**2, i.e. in square form)
!       !!! Output: Fact (nash x nash)
!       subroutine makeFactDtilde(Dtilde, Fact)
! #include "Pointers.fh"
! #include "Input.fh"
! #include "stdalloc.fh"
!         real*8, intent(in) :: Dtilde(ndens2)
!         real*8, intent(out):: Fact(nash(1)**2)
!
!         ! scratch arrays for integrals
!         real*8, allocatable :: ints(:), scr(:)
!         integer :: p,q,t,u, pq
!
!
!         Fact = 0.0d0
!
!         call mma_allocate(scr, ndens2)
!         call mma_allocate(ints, ndens2)
!
!         !!! Coulomb term
!         do t = 1, nAsh(1)
!           do u = 1, nAsh(1)
!             ! get integral batch (pq|tu) for given t,u
!             Call Coul(1,1,1,1,nIsh(1)+t,nIsh(1)+u,ints,Scr)
!
!             ! Fact(tu) = Fact(tu) + sum_pq(2*Dtilde(pq)*(pq|tu))
!             do p = 1, nOrb(1)
!               do q = 1, nOrb(1)
!                 pq = p + (q-1)*nAsh(1)
!                 call daxpy_(nash(1)**2,2*Dtilde(pq),ints(pq),1,Fact,1)
!               end do
!             end do
!           end do
!         end do
!
!         !!! Exchange term
!         do t = 1, nAsh(1)
!           do u = 1, nAsh(1)
!             ! get integral batch (pt|qu) for given t,u
!             Call Exch(1,1,1,1,nIsh(1)+t,nIsh(1)+u,ints,Scr)
!
!             ! Fact(tu) = Fact(tu) - sum_pq(-Dtilde(pq)*(pt|qu))
!             do p = 1, nOrb(1)
!               do q = 1, nOrb(1)
!                 pq = p + (q-1)*nAsh(1)
!                 call daxpy_(nash(1)**2,-Dtilde(pq),ints(pq),1,Fact,1)
!               end do
!             end do
!           end do
!         end do
!
!         !!! I suspect this must be done because of prefactor mismatch
!         !!! between Snyder and e.g. Chaban
!         Fact = 2.0d0*Fact
!         call mma_deallocate(ints)
!         call mma_deallocate(scr)
!
!       end subroutine makeFactDtilde
!
!       !!!                 _
!       !!! Construct Fcore(D) (Eq. 46) and add it to Fact
!       !!! Input: Dbar (norb**2, i.e. in square form)
!       !!! Input: F (Fact(Dtilde))
!       !!! Output: (old F) + Fcore(Dbar) (nash x nash)
!       subroutine addFcoreDbar(Dbar, F)
!
!       use fockmatrices
! #include "Pointers.fh"
! #include "Input.fh"
!       real*8, intent(in) :: Dbar(ndens2)
!       real*8, intent(inout) :: F(nash(1)*nash(1))
!
!       ! offset to the active,core block of Dbar
!       integer :: Doff
!
!       ! offset to the core,active block of Fcore
!       integer :: Foff
!
!       Doff = nish(1)+1
!       Foff = norb(1)*nish(1)+1
!
!       !!! Fcore(Dbar)_tu = sum_t Dbar_tp Fcore_pu
!       !!! F += Fcore(Dbar)
!
!       call dgemm_('N','N',nash(1),nash(1),norb(1),1.0d0,Dbar(Doff),
!      &            nash(1),FI(Foff),norb(1),1.0d0,F,nash(1))
!
!       end subroutine addFcoreDbar
!       !!!                       ~     _
!       !!! Routines to construct D and D from uncompressed kappa (Eqs. 48 and 49)
!       !!! TODO: For efficiency, one can store them in one matrix, but for now I'm going to create two separate matrices
!       !!! and once everything works we can start to optimise the code
!       !!! No symmetry supported
!       !!! D tilde (Eq. 48)
!       !!! In: kappa
!       !!! Out: Dtilde
!       subroutine makeDtilde(kappa, Dtilde)
! #include "Pointers.fh"
! #include "Input.fh"
!         real*8, intent(in) :: kappa(ndens2)
!         real*8, intent(out):: Dtilde(ndens2)
!         integer :: i, j, jj, ij
!
!         Dtilde = 0.0d0
!         ! Dtilde has only k core,act and k core,virt
!         do i = 1, nish(1) ! only for core orbitals
!           do j = 1, norb(1) - nish(1) ! active + virtuals (no deleted orbitals!)
!             jj = nish(1) + j
!             ij = i + (jj-1)*norb(1)
!             Dtilde(ij) = kappa(ij)
!           end do
!         end do
!       end subroutine makeDtilde
!
!       !!! D bar (Eq. 49)
!       !!! In: kappa
!       !!! Out: Dtilde
!       subroutine makeDbar(kappa, Dbar)
! #include "Pointers.fh"
! #include "Input.fh"
!         real*8, intent(in) :: kappa(ndens2)
!         real*8, intent(out):: Dbar(ndens2)
!         integer :: i, j, ii, jj, ij
!
!         Dbar = 0.0d0
!         ! Dbar has -k act,core and k act,virt
!         ! construct -k act,core
!         do i = 1, nash(1) ! only for active orbitals
!           do j = 1, nish(1) ! core orbitals
!             ii = nish(1) + i
!             ij = ii + norb(1)*(j-1)
!             Dbar(ij) = -kappa(ij)
!           end do
!         end do
!
!         ! construct -k act,virt
!         do i = 1, nash(1) ! only for active orbitals
!           do j = 1, norb(1) - nish(1) - nash(1) ! virtual orbitals
!             ii = nish(1) + i
!             jj = nish(1) + nash(1) + j
!             ij = ii + norb(1)*(jj-1)
!             Dbar(ij) = kappa(ij)
!           end do
!         end do
!       end subroutine makeDbar
#endif
      end module dmrg_mini_interface
