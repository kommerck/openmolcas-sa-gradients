************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
************************************************************************
      !!! Calculate Hcc.c
      !!! See Eq. 39 in Snyder et al., or its simpler form
      !!! Hcc.c_PsiI = 2w_Psi (sum_J H_IJ (Cbar_{PsiJ}) - E_Psi Cbar_{PsiI})
      !!! Input: ipCID -- c tilde are stored in work(ipin(ipCID))
      !!! Output: ipS2 -- Hcc.c is stored in work(ipin(ipS2))
      Subroutine Ci_Ci(ipcid,ips2)
#ifdef _DMRG_
      use dmrg_mini_interface
      use qcmaquis_info
      use qcmaquis_interface_measurements
#endif
      Implicit Real*8(A-h,o-z)
#include "WrkSpc.fh"

#include "Input.fh"
#include "Pointers.fh"
#ifdef _DMRG_
#include "dmrginfo_mclr.fh"
#include "stdalloc.fh"
      real*8,allocatable :: mps_red(:), aux_red(:)

      ! Calculate sum_J H_IJ (Cbar_{PsiJ}) -> sigma vector ipS2
      if (doRGLR_MPS) then
        call mma_allocate(mps_red, mpsinfo%n_red)
        call mma_allocate(aux_red, mpsinfo%n_red)
        mps_red = 0.0d0

        do i = 0,nroots-1
          call mps_add_redundant(Work(ipin(ipCId)+i*ncsf(state_sym)),
     &                           aux_red)
          call dmrg_import_sigmavec(
     &             qcm_group_names(1)%states(iroot(i+1)),
     &             'FCIDUMP',mps_red,mpsinfo%n_red,aux_red)
          call mps_remove_redundant(mps_red,
     &               Work(ipin(ipS2)+i*ncsf(state_sym)))
          ! A prefactor of w_Psi before the sigma vector is missing
          ! so we premultiply it here
          call DSCAL_(ncsf(state_sym),Weight(i+1),
     &            Work(ipin(ipS2)+i*ncsf(state_sym)),1)
        end do
      else
#endif
      ! if I understand correctly, in the CI case we don't need
      ! to multiply the sigma vector by state weight?
      Call CISigma_sa(0,state_sym,state_sym,ipFimo,k2int,
     &                    idum,ipCId,ips2,'N')
#ifdef _DMRG_
      end if
#endif
!       write(6,*)" ========  In Ci_CI ======== "
!       write(6,*)"rin_ene",rin_ene,"potnuc",potnuc
      Do i=0,nroots-1
        ! Add -w_PsiE_Psi (?) Cbar_{PsiI}
         EC=(rin_ene+potnuc-ERASSCF(i+1))*Weight(i+1)
!          write(6,*)"EC",EC
       Call Daxpy_(ncsf(State_Sym),EC,
     &            Work(ipin(ipCId)+i*ncsf(state_sym)),1,
     &            Work(ipin(ipS2)+i*ncsf(state_sym)),1)

      End Do

#ifdef _DMRG_
      if (doRGLR_MPS) then
        !!!!!! Add the last term of Eq. 38 in Snyder
        !!!!!! This doesn't seem to affect convergence at all, so it has been removed
        ! S2 -= \sum_Y c_i^Y(E^Y-E^Psi) \sum_j c_j^Y ctilde_j^\Psi

!         ! First build alpha = \sum_j c_j^Y ctilde_j^\Psi
!         alpha = 0.0d0
!         do i = 0, nroots-1 ! i = Psi
!         do j = 0, nroots-1 ! j = Ypsilon
!           alpha = ddot_(ncsf(State_Sym),
!      &     Work(ipin(ipCI)+j*ncsf(state_sym)),1,
!      &     Work(ipin(ipCId)+i*ncsf(state_sym)),1)
!         ! E^Y-E^Psi * weight
!           EC=(rin_ene+potnuc-ERASSCF(i+1)+ERASSCF(j+1))*Weight(i+1)
!         ! S2 -= c_i^Y . alpha . EC
!           Call Daxpy_(ncsf(State_Sym),EC*alpha,
!      &       Work(ipin(ipCI)+j*ncsf(state_sym)),1,
!      &       Work(ipin(ipS2)+i*ncsf(state_sym)),1)
!         end do
!         end do

        if(allocated(mps_red)) call mma_deallocate(mps_red)
        if(allocated(aux_red)) call mma_deallocate(aux_red)
      end if

#endif

      ! multiply the whole term by 2
      Call DSCAL_(nroots*ncsf(state_SYM),2.0d0,Work(ipin(ipS2)),1)
!       write(6,*)" ========  before Ci_CI return ======== "

      Return
      End
