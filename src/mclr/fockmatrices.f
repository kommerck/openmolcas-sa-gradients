!***********************************************************************
! This file is part of OpenMolcas.                                     *
!                                                                      *
! OpenMolcas is free software; you can redistribute it and/or modify   *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! OpenMolcas is distributed in the hope that it will be useful, but it *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!***********************************************************************
! Copyright (C) 2018 Leon Freitag                                      *
!                                                                      *
! Module that contains state-specific active Fock matrices used in     *
! constructing the T matrix and the H x c sigma vector construction for*
! DMRG-SCF gradients. Much better to have a module rather than yet     *
! another common block.                                                *
!                                                                      *
!                                                                      *
! Note! Cholesky isn't supported yet                                   *
!***********************************************************************

      module fockmatrices

      use rdms
      implicit none

      ! FA: Active Fock matrix for the state-average RDM
      ! with the dimension FA(itri(p,q))
      ! p,q as orbital labels
      real*8, allocatable :: FA(:)
! #ifdef _DMRG_
!       ! FAT: Active Fock matrix for state-specific RDMs and TDMs
!       ! to be addressed as FAT(itri(I,J), itri(p,q)) with I,J
!       ! as state labels and p,q as orbital labels
!       real*8, allocatable :: FAT(:,:)
! #endif
      ! Core Fock matrix with the dimension
      ! FI(itri(p,q))
      real*8, allocatable :: FI(:)
      contains


      ! Allocate the memory
      subroutine init_fockmatrices
#include "stdalloc.fh"
#include "Input.fh"
#include "Pointers.fh"
#include "dmrginfo_mclr.fh"
        call mma_allocate(FI, ndens2)
        FI=0.
        call mma_allocate(FA, ndens2)
        FA=0.
! #ifdef _DMRG_
!         if (doRGLR_MPS) then
!           call mma_allocate(FAT, nrootpair, ndens2)
!           FAT=0.
!         end if
! #endif
      end subroutine init_fockmatrices

      ! Deallocate the memory
      subroutine dealloc_fockmatrices
#include "stdalloc.fh"
        if (allocated(FI)) call mma_deallocate(FI)
        if (allocated(FA)) call mma_deallocate(FA)
! #ifdef _DMRG_
!         if (allocated(FAT)) call mma_deallocate(FAT)
! #endif
      end subroutine dealloc_fockmatrices

      ! Build the inactive and active Fock matrices
      ! e.g. from Eq. 4.4 and 4.5 from
      ! Chaban et al. Theor Chem Acc (1997) 97,88-95
      ! It seems that Eqs. 30 and 31
      ! in Bernhardsson et al. Mol Phys. 1999, 96, 617
      ! (which was probably the original paper about MCLR)
      ! have a typo
      ! Note that e.g. Snyder et al, JCP (2017) 146 174113
      ! (eq. 23) has a factor of 2 compared to our
      ! implementation!
      ! Original taken from read22_2.f

      ! Input:
      ! int1: array with 1-electron integrals
      ! DA: (state-average) 1-RDM in trigonal form
      ! Output: FI, FA, FAT matrices in the module
      subroutine build_fockmatrices(int1, DA)
      implicit none
#include "stdalloc.fh"
#include "Pointers.fh"
#include "Input.fh"
#include "WrkSpc.fh"
#include "dmrginfo_mclr.fh"
      integer :: is, jS, kS, lS, ijS, ijB1, iB, jB, nnB, i, lB
      integer :: ist, jst ! counters for states
      real*8 :: fact
      real*8, parameter :: half = 0.5d0, two = 2.0d0, one = 1.0d0
      real*8 int1(*),DA(*)

      ! temporary scratch arrays
      real*8, allocatable :: scr(:), temp2(:)

      call mma_allocate(scr, ndens2)
      call mma_allocate(temp2, ndens2)

      !!! Coulomb part of the Fock matrices
      Do iS=1,nSym
         Do jS=1,iS
            ijS=iEOr(iS-1,jS-1)+1
            Do kS=1,nSym
               Do lS=1,ks
                  If (nOrb(iS)*nOrb(jS)*nOrb(ks)*nOrb(lS).eq.0)
     &                Go To 100
                  If (iEOr(kS-1,lS-1).ne.ijS-1) Goto 100
*                                                                      *
************************************************************************
*                                                                      *
                  ijB1=0
                  Do iB=1,nB(iS)
                     nnB=nB(jS)
                     If (iS.eq.jS) nnB=iB
                     Do jB=1,nnB
*
*                       Read a symmetry block of 2e Coulomb integrals
*                       from disk
*                       Integrals go to Temp2
                        Call COUL(kS,lS,iS,jS,iB,jB,Temp2,Scr)
                        ijB1=ijB1+1
*                                                                      *
************************************************************************
*                                                                      *
*                       Add to unrotated inactive fock matrix
*                        I
*                       F  = sum(i)    2(ii|kl) -(ki|li)
*                        kl
*                                      I
*                       Coulomb term: F  +=2(ii|kl)
*                                      kl
*
                        If (iS.eq.jS.and.iB.eq.jB .and.
     &                     (iB.le.nIsh(iS)))then ! yma change the format for debugging
                           Call DaXpY_(nOrb(kS)*nOrb(lS),Two,
     &                                Temp2,1,FI(ipCM(kS)),1)
                        end if

*                                                                      *
************************************************************************
*                                                                      *
*                       Add to unrotated active fock matrix
*                                      A
*                       Coulomb term: F  =2(ij|kl)d   i<j
*                                      kl          ij
*
                        If (iMethod.eq.iCASSCF) Then
*
                           If (iS.eq.jS) Then
                              If (((iB.gt.nIsh(is)).and.(nAsh(iS).ne.0))
     &                                .and.
     &                            ((jB.gt.nIsh(js)).and.(nAsh(jS).ne.0))
     &                           ) Then
                                 Fact=Two
                                 If (iB.eq.jB) Fact=one
! Construct the Fock matrix for the state-average RDM
! d_ij = dSA_ij
                                 Call DaXpY_(nOrb(kS)*nOrb(lS),
     &                                 Fact*DA(iTri(jB-nIsh(jS)+nA(jS),
     &                                   iB-nIsh(is)+nA(iS))),
     &                                   Temp2,1,FA(ipCM(kS)),1)
! For DMRG: construct the Fock matrices for all densities in rdms::D1
! i.e. state-specific and transition densities
! d_ij = D1(itri(I,J),ij)
! #ifdef _DMRG_
!                                  if (doRGLR_MPS) then
!                                     do ist=1, nroot
!                                     do jst=1,ist
!                                       Call DaXpY_(nOrb(kS)*nOrb(lS),
!      &                                 Fact*D1(itri(ist,jst),
!      &                                   itri(jB-nIsh(jS)+nA(jS),
!      &                                   iB-nIsh(is)+nA(iS))),
!      &                                   Temp2,1,
!      &                                   FAT(itri(ist,jst),
!      &                                   ipCM(kS)),
!      &                                   1)
!                                     end do
!                                     end do
!                                  endif
! #endif

                              End If
                           End If
*
                        End If
*                                                                      *
************************************************************************
*                                                                      *
                     End Do  ! jB
                  End Do     ! iB
*                                                                      *
************************************************************************
*                                                                      *
 100              Continue
               End Do          ! lS
            End Do             ! kS
         End Do                ! jS
      End Do                   ! iS

      !!! Exchange parts

      !!! TODO: Simplify and move into one loop with integrals!!
      Do iS=1,nSym
         kS=iS
         Do js=1,nSym
            lS=jS
            If (iEor(iEor(is-1,js-1),iEor(ks-1,ls-1)).ne.0) Go To 200
            If (nOrb(iS)*nOrb(jS)*nOrb(ks)*nOrb(lS).eq.0)   Go To 200
!             JLB=1
!             JLB1=0
!             JLBas=0
!             jlBB=0
            Do LB=1,nB(LS)
               Do JB=1,nB(JS)
*                                                                      *
************************************************************************
*                                                                      *
                  Call EXCH(is,js,ks,ls,jb,lb,Temp2,Scr)
*                                                                      *
************************************************************************
*                                                                      *
*                 Add to unrotated inactive fock matrix
*                                 I
*                 Exchange term: F  +=-(ij|kj)
*                                 ik
*
                  If (jS.eq.lS.and.jB.eq.lB.and.
     &                (jB.le.nIsh(jS)))
     &               Call DaXpY_(nOrb(iS)*nOrb(kS),-One,
     &                          Temp2,1,FI(ipCM(iS)),1)

*                                                                      *
************************************************************************
*                                                                      *
*                 Add to unrotated active fock matrix
*                                 A
*                 Exchange term: F  +=-1/2(ij|kl)d
*                                 ik              jl
*
                  If (iMethod.eq.iCASSCF) Then
                     If (jS.eq.lS) Then
                        If (((jB.gt.nIsh(js)).and.(nAsh(jS).ne.0)).and.
     &                      ((lB.gt.nIsh(ls)).and.(nAsh(lS).ne.0))
     &                     ) Then
                          Call DaXpY_(nOrb(iS)*nOrb(kS),
     &                         -half*DA(iTri(lB-nIsh(lS)+nA(lS),
     &                                       jB-nIsh(js)+nA(jS))),
     &                               Temp2,1,FA(ipCM(iS)),1)
! ************************************************************************
! *                                                                      *
! ! For DMRG: construct the Fock matrices for all densities in rdms::D1
! ! i.e. state-specific and transition densities
! ! d_ij = D1(itri(I,J),ij)
! ! #ifdef _DMRG_
!                           if (doRGLR_MPS) then
!                             do ist=1, nroot
!                             do jst=1,ist
!                               Call DaXpY_(nOrb(kS)*nOrb(lS),
!      &                             -half*D1(itri(ist,jst),
!      &                               itri(lB-nIsh(lS)+nA(lS),
!      &                                   jB-nIsh(js)+nA(jS))),
!      &                               Temp2,1,
!      &                               FAT(itri(ist,jst),
!      &                               ipCM(iS)),
!      &                               1)
!                             end do
!                             end do
!                           endif
! #endif
                        End If
                     End If
                  End If
*                                                                      *
               End Do
            End Do
 200        Continue
         End Do
      End Do

      !!! Add One-electron part to FI
      !!! TODO: Respect the fact that FI is exactly the same as Work(ipFIMO)
      Call DaXpY_(ndens2,One,int1,1,FI,1)

      ! deallocate scratch
      if (allocated(scr)) call mma_deallocate(scr)
      if (allocated(temp2)) call mma_deallocate(temp2)

      end subroutine build_fockmatrices

!!! Build an active Fock matrix for a given 1-RDM
!!! It's a copy-paste from fockgen.f and very similar to the code
!!! used in build_fockmatrices
!!! However, we're not using build_fa in build_fockmatrices because
!!! it would require re-reading the integrals for every single Fock
!!! matrix

!!! Input:
!!! DA: 1-RDM in square form or in trigonal form
!!! DA_trigonal_: if .TRUE. DA should be read in trigonal form
!!!              if .FALSE. or absent, DA should be read in square form
!!! idSym (optional): target symmetry (for parts that support it)
!!! Output: fock: Active Fock matrix
      subroutine build_fa(DA,fock,idSym_,DA_trigonal_)
      implicit none
#include "Pointers.fh"
#include "Input.fh"
#include "stdalloc.fh"
      real*8,intent(in)  :: fock(*)
      real*8,optional,intent(in) :: DA(*)
      integer,optional,intent(in) :: idSym_
      logical,optional,intent(in) :: DA_trigonal_

      logical :: DA_trigonal
      integer :: idSym

      integer :: ipS, kS, iS, jS, iA, iAA, jA, jAA, kA
      real*8 :: rD

      ! offsets for the fock matrix and exchange integrals
      integer :: ipF, ipM

      real*8, parameter :: half = 0.5d0, two = 2.0d0, one = 1.0d0
      ! temporary scratch arrays
      real*8, allocatable :: scr(:), temp2(:)

      call mma_allocate(scr, ndens2)
      call mma_allocate(temp2, ndens2)

      ! read optional parameters
      if (present(idSym_)) then
        idSym = idSym_
      else
        idSym = 1
      end if

      if (present(DA_trigonal_)) then
        DA_trigonal = DA_trigonal_
      else
        DA_trigonal = .false.
      end if

      Do ipS=1,nSym
         Do kS=1,nSym
            Do iS=1,nSym
               jS=iEor(iEor(ipS-1,kS-1),iS-1)+1
*                                                                      *
************************************************************************
*                                                                      *
*              Coulomb term: F  =(pk|ji)d
*                             kp          ij
*                                                                      *
************************************************************************
*                                                                      *
               If (iEOr(ipS-1,kS-1)+1.eq.iDsym .and.
     &             nBas(ipS)*nIsh(kS).gt.0           ) Then
                  Do iA=1,nAsh(iS)
                     iAA=iA+nIsh(iS)
                     Do jA=1,nAsh(jS)
                        jAA=jA+nIsh(jS)
*
                        Call Coul(ipS,kS,iS,jS,iAA,jAA,
     &                            temp2,scr)
*
                        if (DA_trigonal) then
                          rD=DA(itri(iA+nA(iS),jA+nA(jS)))
                        else
                          rD=DA(iA+nA(iS)+(jA+nA(jS)-1)*nna)
                        end if
                        Call DaXpY_(nBas(ipS)*nIsh(kS),rd,
     &                             temp2,1,Fock(ipMat(ipS,Ks)),1)
*
                     End Do
                  End Do
               End If
*                                                                      *
************************************************************************
*                                                                      *
*              Exchange term: F = -1/2(pk|ji)d
*                              pl             kj
*                                                                      *
************************************************************************
*                                                                      *
               If (iEOr(ipS-1,iS-1)+1.eq.iDsym .and.
     &             nBas(ipS).gt.0                   ) Then
                  Do iA = 1, nIsh(iS)
                     ipF=ipMat(ipS,iS)+nBas(ipS)*(iA-1)
                     Do jA=1,nAsh(jS)
                        jAA=jA+nIsh(jS)
*
                        Call Coul(ipS,kS,iS,jS,iA,jAA,
     &                            temp2,scr)
*
                        ipM=nIsh(kS)*nBas(ipS)
                        Do kA=1,nAsh(kS)
                          if (DA_trigonal) then
                            rd=-Half*DA(itri(kA+nA(kS),jA+nA(jS)))
                          else
                            rd=-Half*DA(kA+nA(kS)+(jA+nA(jS)-1)*nna)
                          end if
                          Call DaXpY_(nBas(ipS),rd,
     &                      temp2(ipM+1),1,Fock(ipF),1)
                          ipM = ipM + nBas(ipS)
                        End Do
*
                     End Do
                  End Do
               End If
*                                                                      *
************************************************************************
*                                                                      *
            End Do
         End Do
      End Do

      ! deallocate scratch
      if (allocated(scr)) call mma_deallocate(scr)
      if (allocated(temp2)) call mma_deallocate(temp2)

      end subroutine build_fa

* Moved from fockgen.f to here to enable optional arguments
      SubRoutine FockGen(d0,rDens1,rdens2,Fock,FockOut,
     &                   idSym,interstate_)
************************************************************************
*                                                                      *
*   Constructs active Fock matrix and Q matrix                         *
*                                                                      *
*   Input: rkappa: Rotation matrix                                     *
*          idsym : symmetry of perturbation                            *
*                                                                      *
*                                                                      *  !!! Implementation of the orbital gradient
*   Output:MO     : MO integrals                                       *  !!! Reconstruction from Helgaker, Molecular Electronic Structure Theory
*          Fock   : Fock matrix (one index transformed integrals)      *  !!! (I hope it's correct)
*          MOtilde: MO (one index transformed integrals)               *
*                                                                      *
************************************************************************
      Implicit Real*8(a-h,o-z)
      Implicit Integer(i-n)
#include "Pointers.fh"
#include "standard_iounits.fh"
#include "Input.fh"
#include "WrkSpc.fh"
#include "real.fh"
#include "sa.fh"
#include "dmrginfo_mclr.fh"
      ! rdens: 1-RDM
      ! rdens2: 2-RDM
      ! Fock: Fock matrix
      ! FockOut: Orbital gradient
      Real*8 Fock(nDens2),FockOut(*), rDens2(*),rDens1(nna,nna)

      ! variable that controls the core Fock matrix and AddGrad2
      ! addition, which are not needed if the Fock matrix/gradient
      ! are build from TDMs for two different states (i.e. for an
      ! interstate coupling)
      ! Consequently, if interstate = .true., the core Fock matrix
      ! is NOT added to the total Fock matrix
      ! default: .false.
      logical, optional, intent(in) :: interstate_
      logical :: interstate

      if (present(interstate_)) then
        interstate = interstate_
      else
        interstate = .false.
      end if
*                                                                      *  !!! First, the Fock matrix is built according to Eqns. 10.8.27-10.8.31
************************************************************************
*                                                                      *
      call dcopy_(nDens2,Zero,0,Fock,1)
*
      n1=0
      Do iS = 1, nSym
        n1=Max(n1,nBas(iS))
      End Do
      n2=n1**2

      ! Conventional 2e integrals
      If (.not.newCho) then
        Call GetMem('ip_MO','Allo','Real',ip_MO,n2)
        Call GetMem('ipScr','Allo','Real',ipScr,n2)
*
*     build the active Fock matrix
        call build_fa(rdens1,Fock,idSym)                                    !!! Build 2 ^A F (right term in 10.8.27)

        !! build_fa builds Fock matrix according to
        !! e.g. from Eq. 4.4 and 4.5 from
        !! Chaban et al. Theor Chem Acc (1997) 97,88-95
        !! Here it's required that it is multiplied by 2
        !! (e.g. as in Snyder et al, JCP (2017) 146 174113, eq. 23)
        Fock = 2*Fock
*                                                                      *
************************************************************************
*                                                                      *
        Call CreQADD(Fock,rdens2,idsym,Work(ip_MO),Work(ipScr),n2)          !!! Build the Q marix (10.8.31 and right term in 10.8.30)
        Call Free_Work(ipScr)
        Call Free_Work(ip_MO)
*
************************************************************************
*       new Cholesky code                                              *
************************************************************************
      else
        nVB=0
        nG2=0
        Do iSym=1,nSym
          nVB = nVB + nAsh(iSym)*nOrb(iSym)
          nAG2=0
          Do jSym=1,nSym
            kSym=iEOr(jsym-1,isym-1)+1
            nAG2=nAg2+nAsh(jSym)*nAsh(kSym)
          End Do
          nG2=nG2+nAG2**2
        End Do

*
**      Unfold 2-DM
*
        Call GetMem('G2x','ALLO','REAL',ipG2x,nG2)
        ipGx=ipG2x
        Do ijS=1,nSym
          Do iS=1,nSym
            jS=iEOR(is-1,ijS-1)+1
            Do kS=1,nSym
              lS=iEOR(kS-1,ijS-1)+1
              Do kAsh=1,nAsh(ks)
                Do lAsh=1,nAsh(ls)
c                 ikl=itri(lAsh+nA(lS),kAsh+nA(kS))
                  ikl=nna*(lAsh+nA(lS)-1)+kAsh+nA(kS)
                  Do iAsh=1,nAsh(is)
                    Do jAsh=1,nAsh(js)
c                     iij =itri(iAsh+nA(is),jAsh+nA(jS))
                      iij=nna*(jAsh+nA(jS)-1)+iAsh+nA(iS)
                      Work(ipGx)=rdens2(itri(iij,ikl))
                      ipGx=ipGx+1
                    End Do
                  End Do
                End Do
              End Do
            End Do
          End Do
        End Do
*
**      Get active CMO
*
        Call GetMem('Cva','Allo','Real',ipAsh,nVB)
        ioff=0
        ioff1=0
        Do iS=1,nSym
          ioff2 = ioff + nOrb(iS)*nIsh(iS)
          Do iB=1,nAsh(iS)
            ioff3=ioff2+nOrb(iS)*(iB-1)
            call dcopy_(nOrb(iS),Work(ipCMO+ioff3),1,
     &                Work(ipAsh+ioff1+iB-1),nAsh(iS))
          End Do
          ioff=ioff+(nIsh(iS)+nAsh(iS))*nOrb(iS)
          ioff1=ioff1+nAsh(iS)*nOrb(iS)
        End Do

*
        Call GetMem('Scr','Allo','Real',ipScr1,n2*2)
        call dcopy_(n2*2,Zero,0,Work(ipScr1),1)
        ipScr2=ipScr1+n2
        ipDA=ip_of_work(rdens1)
        ipFock=ip_of_work(Fock)
*
        Call cho_fock_mclr(ipDA,ipG2x,ipScr1,ipScr2,ipFock,
     &                    ipAsh,ipCMO,nIsh,nAsh,LuAChoVec)
*
        Call GetMem('Scr','Free','Real',ipScr1,n2*2)
        Call GetMem('Cva','Free','Real',ipAsh,nVB)
        Call GetMem('G2x','Free','REAL',ipG2x,nG2)
      EndIf

*
************************************************************************
*       Common part                                                    *
************************************************************************
*
      Do iS=1,nSym                                                         !!! Construct the left term (sum) in (10.8.30)
         If (nBas(iS).gt.0) Then
            jS=iEOr(is-1,iDSym-1)+1
            Do iA=1,nAsh(is)
               Do jA=1,nAsh(js)
                  rd=rDens1(iA+nA(iS),jA+nA(js))
                  ip1=nBas(iS)*(nIsh(is)+iA-1)+ipCM(is)-1
                  ip2=nBas(iS)*(nIsh(js)+jA-1) +ipmat(is,js)
                 Call DaXpY_(nBas(iS),Rd,Work(ipFIMO+ip1),1,Fock(ip2),1)
               End Do
            End Do
         End If
      End Do

*
      If (.not.interstate.and.(iDsym.eq.1)) Then                           !!! Left term in (10.8.27) (2 ^I F)
         Do iS=1,nSym
            If (nBas(iS)*nIsh(iS).gt.0)
     &         Call DaXpY_(nBas(iS)*nIsh(is),Two*d0,
     &                    Work(ipFIMO+ipMat(is,is)-1),1,
     &                    Fock(ipMat(is,is)),1)
         End Do
      End If

*
      Do iS=1,nSym                                                         !!! Build orbital gradient from the Fock matrix: (12.5.5)
         jS=iEOR(iS-1,idSym-1)+1                                           !!! oG_mn = 2(F_mn - F_nm)
         If (nBas(is)*nBas(jS).ne.0)                                       !!!
     &      Call DGeSub(Fock(ipMat(iS,jS)),nBas(iS),'N',                   !!! here we subtract
     &                  Fock(ipMat(jS,iS)),nBas(jS),'T',
     &                  FockOut(ipMat(iS,jS)),nBas(iS),
     &                  nBas(iS),nBas(jS))
      End Do

*
*
      Call DScal_(nDens2,Two,FockOut,1)                                    !!! and here multiply by 2
      If (.not.interstate.and.(iDsym.eq.1))
     &   Call AddGrad2(FockOut,idSym,d0)

*                                                                      *
************************************************************************
*                                                                      *
      return
      end subroutine FockGen


      end module fockmatrices
